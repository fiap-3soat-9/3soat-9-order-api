package main

import (
	"3soat-9-order-api/config"
	"3soat-9-order-api/internal/infra/web/injection"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/httpclient"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/httpserver"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/sql"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/starter"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/validation"
)

func main() {
	starter.Initialize()

	var appConfigs config.Root
	err := starter.UnmarshalConfig(&appConfigs)
	if err != nil {
		panic("error on unmarshall configs")
	}

	config.SetConfig(&appConfigs)

	httpclient.Initialize()
	sql.Initialize()

	//readWriteDB, readOnlyDB := sql.GetClient("readWrite"), sql.GetClient("readOnly")

	//injection.RegisterDomainEvents(readWriteDB, readOnlyDB)
	//injection.RegisterConsumers(readWriteDB, readOnlyDB)

	dependencyInjection := injection.NewDependencyInjection()

	server := httpserver.Builder().
		WithConfig(starter.GetHttpServerConfig()).
		WithHealthCheck(sql.GetHealthChecker()).
		WithControllers(injection.GetAllApis(dependencyInjection)...).
		WithValidator(validation.GetEchoValidator()).
		Build()

	server.Listen()
}
