// Code generated by mockery v2.43.1. DO NOT EDIT.

package mocks

import (
	context "context"

	mock "github.com/stretchr/testify/mock"

	model "3soat-9-order-api/internal/infra/database/model"
)

// PaymentRequestPersistencePort is an autogenerated mock type for the PaymentRequestPersistencePort type
type PaymentRequestPersistencePort struct {
	mock.Mock
}

type PaymentRequestPersistencePort_Expecter struct {
	mock *mock.Mock
}

func (_m *PaymentRequestPersistencePort) EXPECT() *PaymentRequestPersistencePort_Expecter {
	return &PaymentRequestPersistencePort_Expecter{mock: &_m.Mock}
}

// Create provides a mock function with given fields: ctx, paymentRequestCommand
func (_m *PaymentRequestPersistencePort) Create(ctx context.Context, paymentRequestCommand model.PaymentRequests) (interface{}, error) {
	ret := _m.Called(ctx, paymentRequestCommand)

	if len(ret) == 0 {
		panic("no return value specified for Create")
	}

	var r0 interface{}
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, model.PaymentRequests) (interface{}, error)); ok {
		return rf(ctx, paymentRequestCommand)
	}
	if rf, ok := ret.Get(0).(func(context.Context, model.PaymentRequests) interface{}); ok {
		r0 = rf(ctx, paymentRequestCommand)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(interface{})
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, model.PaymentRequests) error); ok {
		r1 = rf(ctx, paymentRequestCommand)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// PaymentRequestPersistencePort_Create_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'Create'
type PaymentRequestPersistencePort_Create_Call struct {
	*mock.Call
}

// Create is a helper method to define mock.On call
//   - ctx context.Context
//   - paymentRequestCommand model.PaymentRequests
func (_e *PaymentRequestPersistencePort_Expecter) Create(ctx interface{}, paymentRequestCommand interface{}) *PaymentRequestPersistencePort_Create_Call {
	return &PaymentRequestPersistencePort_Create_Call{Call: _e.mock.On("Create", ctx, paymentRequestCommand)}
}

func (_c *PaymentRequestPersistencePort_Create_Call) Run(run func(ctx context.Context, paymentRequestCommand model.PaymentRequests)) *PaymentRequestPersistencePort_Create_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(context.Context), args[1].(model.PaymentRequests))
	})
	return _c
}

func (_c *PaymentRequestPersistencePort_Create_Call) Return(_a0 interface{}, _a1 error) *PaymentRequestPersistencePort_Create_Call {
	_c.Call.Return(_a0, _a1)
	return _c
}

func (_c *PaymentRequestPersistencePort_Create_Call) RunAndReturn(run func(context.Context, model.PaymentRequests) (interface{}, error)) *PaymentRequestPersistencePort_Create_Call {
	_c.Call.Return(run)
	return _c
}

// Update provides a mock function with given fields: ctx, paymentReference, status
func (_m *PaymentRequestPersistencePort) Update(ctx context.Context, paymentReference string, status string) error {
	ret := _m.Called(ctx, paymentReference, status)

	if len(ret) == 0 {
		panic("no return value specified for Update")
	}

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, string, string) error); ok {
		r0 = rf(ctx, paymentReference, status)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// PaymentRequestPersistencePort_Update_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'Update'
type PaymentRequestPersistencePort_Update_Call struct {
	*mock.Call
}

// Update is a helper method to define mock.On call
//   - ctx context.Context
//   - paymentReference string
//   - status string
func (_e *PaymentRequestPersistencePort_Expecter) Update(ctx interface{}, paymentReference interface{}, status interface{}) *PaymentRequestPersistencePort_Update_Call {
	return &PaymentRequestPersistencePort_Update_Call{Call: _e.mock.On("Update", ctx, paymentReference, status)}
}

func (_c *PaymentRequestPersistencePort_Update_Call) Run(run func(ctx context.Context, paymentReference string, status string)) *PaymentRequestPersistencePort_Update_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(context.Context), args[1].(string), args[2].(string))
	})
	return _c
}

func (_c *PaymentRequestPersistencePort_Update_Call) Return(_a0 error) *PaymentRequestPersistencePort_Update_Call {
	_c.Call.Return(_a0)
	return _c
}

func (_c *PaymentRequestPersistencePort_Update_Call) RunAndReturn(run func(context.Context, string, string) error) *PaymentRequestPersistencePort_Update_Call {
	_c.Call.Return(run)
	return _c
}

// NewPaymentRequestPersistencePort creates a new instance of PaymentRequestPersistencePort. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
// The first argument is typically a *testing.T value.
func NewPaymentRequestPersistencePort(t interface {
	mock.TestingT
	Cleanup(func())
}) *PaymentRequestPersistencePort {
	mock := &PaymentRequestPersistencePort{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
