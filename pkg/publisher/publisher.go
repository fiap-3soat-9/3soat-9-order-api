package publisher

import (
	"context"
	"github.com/rs/zerolog"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/logger"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/messaging/message"
	publisher2 "gitlab.com/fiap-3soat-9/go-pkg-soat3/messaging/sns/publisher"
)

type Publisher[T any] interface {
	Publish(
		ctx context.Context,
		topicName string,
		payload T,
		messageAttributes map[string]struct{ Type, Value string },
	) error
}

type SNSPublisher[T any] struct {
	logger zerolog.Logger
}

func (p SNSPublisher[T]) Publish(
	ctx context.Context,
	topicName string,
	payload T,
	messageAttributes map[string]struct{ Type, Value string },
) error {
	err := publisher2.NewSNSPublisher(
		context.TODO(),
		topicName,
		message.JsonSerializer[T]{Payload: payload},
	).WithMessageAttributes(
		messageAttributes,
	).Publish()
	if err != nil {
		p.logger.Error().
			Ctx(ctx).
			Err(err).
			Str("topic", topicName).
			Msg("Error on sent notification to topic")

		return err
	}
	return nil
}

func NewPublisher[T any]() Publisher[T] {
	return SNSPublisher[T]{
		logger: logger.Get(),
	}
}
