package output

import (
	"3soat-9-order-api/internal/domain"
	"context"
	"github.com/google/uuid"
)

type OrderPersistencePort interface {
	Create(ctx context.Context, order domain.Order) error
	FindAll(ctx context.Context) ([]domain.Order, error)
	FindByStatus(ctx context.Context, status string) ([]domain.Order, error)
	FindByNumber(ctx context.Context, number int) (*domain.Order, error)
	FindById(ctx context.Context, orderId uuid.UUID) (*domain.Order, error)
	Update(ctx context.Context, order domain.Order) error
	UpdateStatus(ctx context.Context, orderId string, newStatus string) error
}
