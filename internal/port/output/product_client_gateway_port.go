package output

import (
	"3soat-9-order-api/internal/domain"
	"context"
	"github.com/google/uuid"
)

type ProductClientGatewayPort interface {
	Create(ctx context.Context, product domain.Product) error
	Update(ctx context.Context, product domain.Product) error
	GetAll(ctx context.Context) ([]domain.Product, error)
	GetByID(ctx context.Context, productID uuid.UUID) (*domain.Product, error)
	GetByNumber(ctx context.Context, productNumber int) (*domain.Product, error)
	GetByCategory(ctx context.Context, productID string) ([]domain.Product, error)
	CheckProductExists(ctx context.Context, product domain.Product) (*domain.Product, error)
}
