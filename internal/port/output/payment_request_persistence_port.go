package output

import (
	"3soat-9-order-api/internal/infra/database/model"
	"context"
)

type PaymentRequestPersistencePort interface {
	Create(ctx context.Context, paymentRequestCommand model.PaymentRequests) (any, error)
	Update(ctx context.Context, paymentReference, status string) error
}
