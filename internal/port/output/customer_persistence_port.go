package output

import (
	"3soat-9-order-api/internal/domain"
	"context"
)

type CustomerClientPort interface {
	Create(ctx context.Context, customer domain.Customer) error
	Get(ctx context.Context, document string) (*domain.Customer, error)
}
