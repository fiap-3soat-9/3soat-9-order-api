package output

import (
	"3soat-9-order-api/internal/domain"
	"3soat-9-order-api/internal/usecase/result"
	"context"
)

type PaymentClientGatewayPort interface {
	Create(ctx context.Context, order domain.Order) (*result.PaymentCreatedResult, error)
}
