package input

import (
	"3soat-9-order-api/internal/domain/valueobject"
	"context"
	"github.com/google/uuid"
)

type UpdateOrderPort interface {
	Update(
		ctx context.Context,
		orderId uuid.UUID,
		status valueobject.OrderStatus,
		paymentId *uuid.UUID,
	) error
}
