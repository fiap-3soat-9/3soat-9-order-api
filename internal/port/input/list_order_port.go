package input

import (
	"3soat-9-order-api/internal/usecase/result"
	"context"
)

type ListOrderPort interface {
	FindAllOrders(ctx context.Context) ([]result.ListOrderResult, error)
	FindByStatus(ctx context.Context, status string) ([]result.ListOrderResult, error)
	FindByNumber(ctx context.Context, number int) (*result.ListOrderResult, error)
}
