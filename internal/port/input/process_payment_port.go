package input

import (
	"3soat-9-order-api/internal/domain"
	"3soat-9-order-api/internal/usecase/result"
	"context"
)

type ProcessPaymentPort interface {
	ProcessPayment(ctx context.Context, order domain.Order) (*result.PaymentCreatedResult, error)
	PublishToProcessPayment(ctx context.Context, order domain.Order) (*result.PaymentCreatedResult, error)
}
