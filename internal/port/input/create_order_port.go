package input

import (
	"3soat-9-order-api/internal/usecase/command"
	"3soat-9-order-api/internal/usecase/result"
	"context"
)

type CreateOrderPort interface {
	AddOrder(ctx context.Context, createOrderCommand command.CreateOrderCommand) (*result.CreateOrderResult, error)
}
