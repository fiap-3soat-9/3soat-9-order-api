package domain

type ProductCategory struct {
	Name                    string
	AcceptCustom            bool
	ConfigByProductCategory []IngredientTypeProductCategory
}
