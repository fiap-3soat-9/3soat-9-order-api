package domain

import (
	"3soat-9-order-api/internal/domain/valueobject"
	"github.com/google/uuid"
	"time"
)

type OrderHistory struct {
	Id        uuid.UUID
	OrderId   uuid.UUID
	Status    valueobject.OrderStatus
	ChangeBy  string
	CreatedAt time.Time
}
