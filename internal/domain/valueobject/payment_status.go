package valueobject

type Status string

const (
	PaymentCreated  Status = "created"
	PaymentApproved Status = "approved"
	PaymentRejected Status = "rejected"
)
