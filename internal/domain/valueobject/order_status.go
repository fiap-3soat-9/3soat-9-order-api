package valueobject

import (
	"fmt"
)

type OrderStatus string

const (
	Created        OrderStatus = "CREATED"
	WaitPayment    OrderStatus = "WAITING_PAYMENT"
	PaymentRefused OrderStatus = "PAYMENT_REFUSED"
	Started        OrderStatus = "IN_PREPARATION"
	Paid           OrderStatus = "PAID"
	Ready          OrderStatus = "READY"
	Completed      OrderStatus = "COMPLETED"
)

var orderStatusDescriptions = map[OrderStatus]string{
	Created:        "CREATED",
	WaitPayment:    "WAITING_PAYMENT",
	PaymentRefused: "PAYMENT_REFUSED",
	Started:        "IN_PREPARATION",
	Paid:           "PAID",
	Ready:          "READY",
	Completed:      "COMPLETED",
}

func (os OrderStatus) Order() (int, error) {
	switch os {
	case Ready:
		return 1, nil
	case Started:
		return 2, nil
	case WaitPayment:
		return 3, nil
	case Created:
		return 4, nil
	default:
		return 99, fmt.Errorf("invalid order status: %v", os)
	}
}

func (os OrderStatus) String() string {
	desc, ok := orderStatusDescriptions[os]
	if !ok {
		return "UNKNOWN"
	}
	return desc
}
