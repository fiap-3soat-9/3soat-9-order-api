package domain

import (
	"3soat-9-order-api/internal/domain/valueobject"
	"github.com/google/uuid"
	"time"
)

type Order struct {
	Id         uuid.UUID
	Number     int
	CustomerId string
	Products   []OrderProduct
	CreatedAt  time.Time
	UpdatedAt  time.Time
	Status     valueobject.OrderStatus
	Amount     int
	History    []OrderHistory
	PaymentId  uuid.UUID
}

type OrderProduct struct {
	Name     string
	Id       uuid.UUID
	Product  Product
	OrderId  uuid.UUID
	Quantity int
	Amount   int
}
