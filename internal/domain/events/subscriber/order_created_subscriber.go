package subscriber

import (
	"3soat-9-order-api/config"
	"3soat-9-order-api/internal/domain/events"
	"3soat-9-order-api/internal/infra/database"
	"3soat-9-order-api/internal/infra/database/model"
	"3soat-9-order-api/internal/port/output"
	"3soat-9-order-api/internal/usecase/command"
	"3soat-9-order-api/pkg/domainevent"
	"3soat-9-order-api/pkg/publisher"
	"context"
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/logger"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/messaging/message"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/sql"
	"sync"
	"time"
)

var (
	orderCreatedEventHandlerInstance *OrderCreatedEventHandler
	orderCreatedEventHandlerOnce     sync.Once
)

const (
	OrderCreatedTopic = "ORDER_EVENTS"
	PaymentTopic      = "PAYMENT_EVENTS"

	OrderCreatedEventType         = "order.created"
	PaymentCreateCommandEventType = "payment.create"
)

//func init() {
//	NewOrderCreatedEventHandler(
//		database.GetOrderPersistenceGateway(sql.GetClient("readWrite"),
//		sql.GetClient("readOnly"), logger.Get()),
//		publisher.NewPublisher[message.Message[command.CreatePaymentCommand]](),
//	)
//}

type OrderCreatedEventHandler struct {
	orderPersistenceGateway output.OrderPersistencePort
	publisher               publisher.Publisher[message.Message[command.CreatePaymentCommand]]

	//processPaymentUseCase   input.ProcessPaymentPort
}

func NewOrderCreatedEventHandler(orderPersistenceGateway output.OrderPersistencePort, pb publisher.Publisher[message.Message[command.CreatePaymentCommand]]) *OrderCreatedEventHandler {
	orderCreatedEventHandlerOnce.Do(func() {
		orderCreatedEventHandlerInstance = &OrderCreatedEventHandler{
			orderPersistenceGateway: orderPersistenceGateway,
			publisher:               pb,
		}
	})
	return orderCreatedEventHandlerInstance
}

// Handle processes the OrderCreatedDomainEvent
func (h OrderCreatedEventHandler) Handle(ctx context.Context, event domainevent.Event[events.OrderCreatedDomainEvent]) error {
	orderEvent := event.GetPayload()

	fmt.Printf("Handling OrderCreated event: %s\n", event.GetName())
	//readWriteDB, readOnlyDB := sql.GetClient("readWrite"), sql.GetClient("readOnly")

	//database.GetOrderPersistenceGateway(readWriteDB, readOnlyDB, logger.Get())

	//orderFound, _ := h.orderPersistenceGateway.FindById(ctx, orderEvent.Id)
	//if orderFound == nil {
	//	return fmt.Errorf("order not found")
	//}

	readWriteDB, readOnlyDB := sql.GetClient("readWrite"), sql.GetClient("readOnly")
	paymentPersistence := database.GetPaymentRequestPersistence(readWriteDB, readOnlyDB, logger.Get())

	paymentRequestReference := uuid.NewString()
	_, err := paymentPersistence.Create(ctx, model.PaymentRequests{
		ID:          uuid.NewString(),
		ReferenceID: paymentRequestReference,
		OrderID:     orderEvent.Id.String(),
		Amount:      int64(orderEvent.Amount),
		Status:      "Requested",
		Attempts:    1,
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	})
	if err != nil {
		return err
	}

	var orderItems []command.OrderItem
	for _, orderProduct := range orderEvent.Products {
		orderItems = append(orderItems, command.OrderItem{
			Name:        orderProduct.Product.Name,
			Amount:      orderProduct.Product.Amount,
			Quantity:    orderProduct.Quantity,
			TotalAmount: orderProduct.Amount,
		})
	}

	var orderCreatedCommand = command.CreatePaymentCommand{
		ReferenceID: paymentRequestReference,
		Amount:      orderEvent.Amount,
		OrderId:     orderEvent.Id,
		OrderItems:  orderItems,
	}

	messageTemplate := *message.NewMessageWrapper[command.CreatePaymentCommand]().
		WithBody(orderCreatedCommand).
		WithTopic(PaymentTopic).
		WithSource(config.GetConfig().Application.Name).
		WithTopicEvent(PaymentCreateCommandEventType)

	messageAttributes := map[string]struct{ Type, Value string }{
		"event_type": {Type: "String", Value: PaymentCreateCommandEventType},
	}

	name := config.GetConfig().Sns.Topics.PaymentEvents.Name
	if err := publisher.NewPublisher[command.CreatePaymentCommand]().Publish(
		ctx,
		name,
		messageTemplate.Body.Contents,
		messageAttributes,
	); err != nil {
		return err
	}
	fmt.Printf("Sent %s command to topic %s, orderID: %s referenceID: %s\n", PaymentCreateCommandEventType, PaymentTopic, orderEvent.Id.String(), paymentRequestReference)

	//err := h.publisher.Publish(
	//	ctx,
	//	OrderCreatedTopic,
	//	messageTemplate,
	//	messageAttributes,
	//)
	//if err != nil {
	//	return err
	//}

	return nil
}
