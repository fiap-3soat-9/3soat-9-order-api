package events

import (
	"3soat-9-order-api/internal/domain"
	"3soat-9-order-api/internal/domain/valueobject"
	"3soat-9-order-api/pkg/domainevent"
	"github.com/google/uuid"
	"time"
)

type OrderCreatedDomainEvent struct {
	Id         uuid.UUID
	Number     int
	CustomerId string
	Products   []domain.OrderProduct
	CreatedAt  time.Time
	UpdatedAt  time.Time
	Status     valueobject.OrderStatus
	Amount     int
}

func (o OrderCreatedDomainEvent) ToDomainEvent() domainevent.Event[OrderCreatedDomainEvent] {
	return domainevent.NewDomainEvent("order.created", o)

}
