package domain

type Product struct {
	Number      int
	Name        string
	Amount      int
	Description string
	Category    string
	ImgPath     string
	Ingredients []ProductIngredient
	Active      bool
}

type ProductIngredient struct {
	Number   int
	Name     string
	Amount   int
	Quantity int
}
