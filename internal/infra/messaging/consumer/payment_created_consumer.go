package consumer

import (
	"3soat-9-order-api/config"
	"3soat-9-order-api/internal/domain/valueobject"
	"3soat-9-order-api/internal/infra/database/model"
	"3soat-9-order-api/internal/port/output"
	"context"
	"fmt"
	"github.com/google/uuid"
	"github.com/rs/zerolog"
	"sync"
	"time"

	"gitlab.com/fiap-3soat-9/go-pkg-soat3/messaging/sqs/consumer"
)

var (
	paymentCreatedConsumerInstance = &PaymentCreatedConsumer{}
	paymentCreatedConsumerOnce     sync.Once
)

type PaymentCreatedConsumer struct {
	orderPersistenceGateway   output.OrderPersistencePort
	paymentRequestPersistence output.PaymentRequestPersistencePort
	logger                    zerolog.Logger
}

func (p PaymentCreatedConsumer) HandlerError(ctx context.Context, errChan <-chan error) {
	for err := range errChan {
		p.logger.Warn().Ctx(ctx).Err(err).Msg("Failed on consumer message")
	}
}

func (p PaymentCreatedConsumer) Handler(ctx context.Context, msg consumer.Message[PaymentCreatedMessage]) error {
	queueName := config.GetConfig().Queues.PaymentCreated

	body, err := msg.ReadBody()
	if err != nil {
		fmt.Println("Error on parse message, causa: " + err.Error())
		return err
	}

	content := body

	fmt.Printf("Consuming message from %s\n", queueName)

	//marshal, err := json.Marshal(body.Body)
	//if err != nil {
	//	return err
	//}

	fmt.Printf("Processing payment:  %s for order: %s\n", content.PaymentID, content.OrderID)

	_, err = p.paymentRequestPersistence.Create(ctx, model.PaymentRequests{
		ID:          uuid.NewString(),
		ReferenceID: content.PaymentID.String(),
		OrderID:     content.OrderID.String(),
		Amount:      int64(6000),
		Status:      string(valueobject.PaymentCreated),
		Attempts:    1,
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	})
	if err != nil {
		return err
	}

	err = p.orderPersistenceGateway.UpdateStatus(ctx, content.OrderID.String(), valueobject.WaitPayment.String())
	if err != nil {
		return err
	}
	fmt.Printf("payment:  %s created to order: %s\n", body.PaymentID, body.OrderID)
	time.Sleep(500 * time.Millisecond)

	fmt.Printf("Sent customer.waiting_payment command to topic CUSTOMER_EVENTS, orderID: %s \n", body.OrderID)
	return err
}

func NewPaymentCreatedConsumer(
	logger zerolog.Logger,
	orderPersistenceGateway output.OrderPersistencePort,
	paymentRequestPersistence output.PaymentRequestPersistencePort,
) *PaymentCreatedConsumer {
	paymentCreatedConsumerOnce.Do(func() {
		paymentCreatedConsumerInstance = &PaymentCreatedConsumer{
			orderPersistenceGateway:   orderPersistenceGateway,
			paymentRequestPersistence: paymentRequestPersistence,
			logger:                    logger,
		}
	})

	return paymentCreatedConsumerInstance

}
