package consumer

import (
	"3soat-9-order-api/config"
	"3soat-9-order-api/internal/domain/valueobject"
	"3soat-9-order-api/internal/port/output"
	"context"
	"fmt"
	"github.com/rs/zerolog"
	"sync"
	"time"

	"gitlab.com/fiap-3soat-9/go-pkg-soat3/messaging/sqs/consumer"
)

var (
	paymentConsumerInstance = &PaymentUpdatedConsumer{}
	paymentConsumerOnce     sync.Once
)

type PaymentUpdatedConsumer struct {
	orderPersistenceGateway   output.OrderPersistencePort
	paymentRequestPersistence output.PaymentRequestPersistencePort
	logger                    zerolog.Logger
}

func (p PaymentUpdatedConsumer) HandlerError(ctx context.Context, errChan <-chan error) {
	for err := range errChan {
		p.logger.Warn().Ctx(ctx).Err(err).Msg("Failed on consumer message")
	}
}

func (p PaymentUpdatedConsumer) Handler(ctx context.Context, msg consumer.Message[PaymentUpdatedMessage]) error {
	queueName := config.GetConfig().Queues.PaymentUpdated

	body, err := msg.ReadBody()
	if err != nil {
		return err
	}

	content := body

	fmt.Printf("Consuming event from %s\n", "payment."+body.Status)
	err = p.paymentRequestPersistence.Update(ctx, content.PaymentID.String(), content.Status)
	if err != nil {
		return err
	}

	switch content.Status {
	case "created":
		fmt.Printf("Processing payment:  %s for order: %s\n", content.PaymentID, content.OrderID)
		err = p.orderPersistenceGateway.UpdateStatus(ctx, content.OrderID.String(), valueobject.WaitPayment.String())
		if err != nil {
			return err
		}
		fmt.Printf("Payment created:  %s for order: %s\n", content.PaymentID, content.OrderID)
		time.Sleep(500 * time.Millisecond)
		fmt.Println("Publish to topic: CUSTOMER_EVENTS event_type: PaymentCreatedNotification")
		break
	case "approved":
		err = p.orderPersistenceGateway.UpdateStatus(ctx, content.OrderID.String(), valueobject.Paid.String())
		if err != nil {
			return err
		}
		fmt.Printf("Payment approved:  %s for order: %s\n", content.PaymentID, content.OrderID)
		time.Sleep(500 * time.Millisecond)
		fmt.Println("Order Paid, Publish to topic: KITCHEN_EVENTS event_type: KitchenPrepareOrderCommand")
		break
	case "rejected":
		err = p.orderPersistenceGateway.UpdateStatus(ctx, content.OrderID.String(), valueobject.PaymentRefused.String())
		if err != nil {
			return err
		}
		fmt.Printf("Payment rejected:  %s for order: %s\n", content.PaymentID, content.OrderID)
		time.Sleep(500 * time.Millisecond)
		fmt.Println("Publish to topic: CUSTOMER_EVENTS event_type: PaymentRejectedNotification")
		break
	case "failed":
		err = p.orderPersistenceGateway.UpdateStatus(ctx, content.OrderID.String(), valueobject.PaymentRefused.String())
		if err != nil {
			return err
		}
		break
	default:
		p.logger.Info().Str("queueName", queueName).Interface("content", content).Msg("payment status not supported")
	}

	return err
}

func NewPaymentUpdatedConsumer(
	logger zerolog.Logger,
	orderPersistenceGateway output.OrderPersistencePort,
	paymentRequestPersistence output.PaymentRequestPersistencePort,
) *PaymentUpdatedConsumer {
	paymentConsumerOnce.Do(func() {
		paymentConsumerInstance = &PaymentUpdatedConsumer{
			orderPersistenceGateway:   orderPersistenceGateway,
			paymentRequestPersistence: paymentRequestPersistence,
			logger:                    logger,
		}
	})

	return paymentConsumerInstance

}
