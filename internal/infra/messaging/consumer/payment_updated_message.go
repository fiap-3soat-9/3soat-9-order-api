package consumer

import (
	"github.com/google/uuid"
)

type PaymentUpdatedMessage struct {
	Id        uuid.UUID `json:"id"`
	OrderID   uuid.UUID `json:"order_id"`
	Status    string    `json:"status"`
	PaymentID uuid.UUID `json:"payment_id"`
}

type PaymentCreatedMessage struct {
	ID          uuid.UUID `json:"id"`
	OrderID     uuid.UUID `json:"order_id"`
	Status      string    `json:"status"`
	PaymentID   uuid.UUID `json:"payment_id"`
	Amount      int       `json:"amount"`
	PaymentData string    `json:"payment_data"`
	//CreatedAt   time.Time `json:"created-at"`
}
