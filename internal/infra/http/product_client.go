package http

import (
	"3soat-9-order-api/config"
	"3soat-9-order-api/internal/domain"
	response2 "3soat-9-order-api/internal/infra/http/response"
	"context"
	"fmt"
	"github.com/google/uuid"
	"github.com/rs/zerolog"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/httpclient"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/logger"
	"strconv"
)

type ProductClientGateway struct {
	client httpclient.Client
	logger zerolog.Logger
}

func (p ProductClientGateway) Create(ctx context.Context, product domain.Product) error {
	p.logger.Info().Msg("create product....")
	return nil
}

func (p ProductClientGateway) Update(ctx context.Context, product domain.Product) error {
	//TODO implement me
	panic("implement me")
}

func (p ProductClientGateway) GetAll(ctx context.Context) ([]domain.Product, error) {
	//TODO implement me
	panic("implement me")
}

func (p ProductClientGateway) GetByID(ctx context.Context, productID uuid.UUID) (*domain.Product, error) {
	//TODO implement me
	panic("implement me")
}

func (p ProductClientGateway) GetByNumber(ctx context.Context, productNumber int) (*domain.Product, error) {
	gatewayProp := config.GetConfig().ProductGateway
	httpRequest := httpclient.NewRequest[response2.FindProductWithIngredients](
		ctx, p.client, gatewayProp.FindProductByNumberUri,
	).
		WithPathParams(map[string]string{
			"number": strconv.Itoa(productNumber),
		})

	response, err := httpRequest.Get()

	if err != nil {
		return nil, err
	}

	fmt.Printf("product: %s founded\n", fmt.Sprintf("%s", response.Result.Description))
	//p.logger.Info().Msg("product: " + fmt.Sprintf("%s", response.Result.Description) + " founded")

	return response.Result.ToDomain(), nil
}

func (p ProductClientGateway) GetByCategory(ctx context.Context, productID string) ([]domain.Product, error) {
	//TODO implement me
	panic("implement me")
}

func (p ProductClientGateway) CheckProductExists(ctx context.Context, product domain.Product) (*domain.Product, error) {
	//TODO implement me
	panic("implement me")
}

func GetProductClient() *ProductClientGateway {
	return &ProductClientGateway{
		logger: logger.Get(),
		client: httpclient.GetClient("productGateway"),
	}
}
