package response

import "3soat-9-order-api/internal/domain"

type FindProductWithIngredients struct {
	Name        string                    `json:"name"`
	Number      int                       `json:"number"`
	Amount      int                       `json:"amount"`
	Description string                    `json:"description"`
	Category    string                    `json:"category"`
	ImgPath     string                    `json:"imgPath"`
	Ingredients []FindProductsIngredients `json:"ingredients"`
	Active      bool                      `json:"active"`
}

type FindProductsIngredients struct {
	Number   int    `json:"number"`
	Name     string `json:"name"`
	Amount   int    `json:"amount"`
	Quantity int    `json:"quantity"`
}

func (pi FindProductWithIngredients) ToDomain() *domain.Product {
	return &domain.Product{
		Number:      pi.Number,
		Name:        pi.Name,
		Amount:      pi.Amount,
		Description: pi.Description,
		Category:    pi.Category,
		ImgPath:     pi.ImgPath,
		Ingredients: toIngredientsDomain(pi.Ingredients),
		Active:      pi.Active,
	}
}

func toIngredientsDomain(ingredients []FindProductsIngredients) []domain.ProductIngredient {
	var ingredientsDomain []domain.ProductIngredient

	for _, ingredient := range ingredients {
		ingredientsDomain = append(ingredientsDomain, domain.ProductIngredient{
			Number:   ingredient.Number,
			Name:     ingredient.Name,
			Amount:   ingredient.Amount,
			Quantity: ingredient.Quantity,
		})
	}
	return ingredientsDomain
}
