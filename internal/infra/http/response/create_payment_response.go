package response

import (
	"github.com/google/uuid"
	"time"
)

type PaymentCreatedResponse struct {
	PaymentId   uuid.UUID `json:"payment_id"`
	OrderId     uuid.UUID `json:"order_id"`
	PaymentData string    `json:"payment_data"`
	CreatedAt   time.Time `json:"created_at"`
}
