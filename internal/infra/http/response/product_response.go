package response

import (
	"3soat-9-order-api/internal/domain"
	"github.com/google/uuid"
	"time"
)

type ProductResponse struct {
	ID          uuid.UUID
	Number      int
	Name        string
	Amount      int
	Description string
	Category    ProductCategoryResponse
	Menu        bool
	ImgPath     string
	CreatedAt   time.Time
	UpdatedAt   time.Time
	Ingredients []ProductIngredientResponse
	Active      bool
}

func (p ProductResponse) ToIngredientsDomain(ingredients []ProductIngredientResponse) []domain.ProductIngredient {
	var ingredientsDomain []domain.ProductIngredient

	for _, ingredient := range ingredients {
		pid := &domain.ProductIngredient{
			Number:   ingredient.Ingredient.Number,
			Name:     ingredient.Ingredient.Name,
			Quantity: ingredient.Quantity,
			Amount:   ingredient.Amount,
		}
		ingredientsDomain = append(ingredientsDomain, *pid)
	}

	return ingredientsDomain
}

type ProductIngredientResponse struct {
	ID         uuid.UUID
	ProductId  uuid.UUID
	Ingredient IngredientResponse
	Quantity   int
	Amount     int
}

func ToDomainIngredient(ing ProductIngredientResponse) domain.Ingredient {

	return domain.Ingredient{
		ID:     ing.Ingredient.ID,
		Number: ing.Ingredient.Number,
		Name:   ing.Ingredient.Name,
		Amount: ing.Amount,
		Type:   ToDomainIngredientType(ing.Ingredient.Type),
	}
}

func ToDomainIngredientType(igt IngredientTypeResponse) domain.IngredientType {
	var configProductCategory []domain.IngredientTypeProductCategory
	for _, cpg := range igt.ConfigByProductCategory {
		configProductCategory = append(configProductCategory, domain.IngredientTypeProductCategory{
			ID:              cpg.ID,
			IngredientType:  cpg.IngredientType,
			Optional:        cpg.Optional,
			MaxQtd:          cpg.MaxQtd,
			ProductCategory: cpg.ProductCategory,
		})
	}
	return domain.IngredientType{
		Name:                    igt.Name,
		ConfigByProductCategory: configProductCategory,
	}
}

type IngredientResponse struct {
	ID     uuid.UUID
	Number int
	Name   string
	Amount int
	Type   IngredientTypeResponse
}

type ProductCategoryResponse struct {
	Name                    string
	AcceptCustom            bool
	ConfigByProductCategory []IngredientTypeProductCategoryResponse
}

func (p ProductCategoryResponse) ToDomainProductCategory() domain.ProductCategory {
	var configProductCategory []domain.IngredientTypeProductCategory
	for _, cpg := range p.ConfigByProductCategory {
		configProductCategory = append(configProductCategory, domain.IngredientTypeProductCategory{
			ID:              cpg.ID,
			IngredientType:  cpg.IngredientType,
			Optional:        cpg.Optional,
			MaxQtd:          cpg.MaxQtd,
			ProductCategory: cpg.ProductCategory,
		})
	}
	return domain.ProductCategory{
		Name:                    p.Name,
		AcceptCustom:            p.AcceptCustom,
		ConfigByProductCategory: configProductCategory,
	}
}

type IngredientTypeResponse struct {
	Name                    string
	ConfigByProductCategory []IngredientTypeProductCategoryResponse
}

type IngredientTypeProductCategoryResponse struct {
	ID              uuid.UUID
	IngredientType  string
	Optional        bool
	MaxQtd          int
	ProductCategory string
}

//type ProductResponseV2 struct {
//	ID          string `json:"ID"`
//	Number      int    `json:"Number"`
//	Name        string `json:"Name"`
//	Amount      int    `json:"Amount"`
//	Description string `json:"Description"`
//	Category    struct {
//		Name                    string `json:"Name"`
//		AcceptCustom            bool   `json:"AcceptCustom"`
//		ConfigByProductCategory []struct {
//			ID              string `json:"ID"`
//			IngredientType  string `json:"IngredientType"`
//			Optional        bool   `json:"Optional"`
//			MaxQtd          int    `json:"MaxQtd"`
//			ProductCategory string `json:"ProductCategory"`
//		} `json:"ConfigByProductCategory"`
//	} `json:"Category"`
//	Menu        bool      `json:"Menu"`
//	ImgPath     string    `json:"ImgPath"`
//	CreatedAt   time.Time `json:"CreatedAt"`
//	UpdatedAt   time.Time `json:"UpdatedAt"`
//	Ingredients []struct {
//		ID         string `json:"ID"`
//		ProductId  string `json:"ProductId"`
//		Ingredient struct {
//			ID     string `json:"ID"`
//			Number int    `json:"Number"`
//			Name   string `json:"Name"`
//			Amount int    `json:"Amount"`
//			Type   struct {
//				Name                    string `json:"Name"`
//				ConfigByProductCategory []struct {
//					ID              string `json:"ID"`
//					IngredientType  string `json:"IngredientType"`
//					Optional        bool   `json:"Optional"`
//					MaxQtd          int    `json:"MaxQtd"`
//					ProductCategory string `json:"ProductCategory"`
//				} `json:"ConfigByProductCategory"`
//			} `json:"Type"`
//		} `json:"Ingredient"`
//		Quantity int `json:"Quantity"`
//		Amount   int `json:"Amount"`
//	} `json:"Ingredients"`
//	Active bool `json:"Active"`
//}
