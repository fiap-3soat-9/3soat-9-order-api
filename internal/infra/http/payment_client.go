package http

import (
	"3soat-9-order-api/config"
	"3soat-9-order-api/internal/domain"
	response2 "3soat-9-order-api/internal/infra/http/response"
	"3soat-9-order-api/internal/usecase/result"
	"3soat-9-order-api/pkg/publisher"
	"context"
	"fmt"
	"github.com/rs/zerolog"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/httpclient"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/logger"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/messaging/message"
)

type PaymentClientGateway struct {
	client httpclient.Client
	publisher.Publisher[message.Message[response2.PaymentCreatedResponse]]
	logger zerolog.Logger
}

func (p PaymentClientGateway) Create(ctx context.Context, order domain.Order) (*result.PaymentCreatedResult, error) {
	fmt.Println("Creating payment for order: " + order.Id.String())

	path := config.GetConfig().PaymentGateway

	p.logger.Println("Creating payment for order: " + order.Id.String() + " URI: " + path.CreatePaymentUri)

	httpRequest := httpclient.NewRequest[response2.PaymentCreatedResponse](
		ctx, p.client, path.CreatePaymentUri,
	)

	response, err := httpRequest.Post(httpRequest)

	if err != nil {
		return nil, err
	}
	return &result.PaymentCreatedResult{
		PaymentId:   response.Result.PaymentId,
		OrderId:     response.Result.OrderId,
		PaymentData: response.Result.PaymentData,
		CreatedAt:   response.Result.CreatedAt,
	}, nil
}

func GetPaymentClient() *PaymentClientGateway {
	return &PaymentClientGateway{
		logger: logger.Get(),
	}
}
