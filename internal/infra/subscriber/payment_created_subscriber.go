package subscriber

import (
	"3soat-9-order-api/internal/domain/events"
	"3soat-9-order-api/internal/port/output"
	"3soat-9-order-api/pkg/domainevent"
	"context"
)

type PaymentCreatedEventHandler struct {
	orderPersistence output.OrderPersistencePort
}

func (p PaymentCreatedEventHandler) Handle(ctx context.Context, event domainevent.Event[events.PaymentCreatedDomainEvent]) error {
	//fmt.Println(event.GetPayload().Status)
	return nil
}
