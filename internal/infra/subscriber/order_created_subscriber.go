package subscriber

import (
	"3soat-9-order-api/internal/domain/events"
	"3soat-9-order-api/internal/port/output"
	"3soat-9-order-api/pkg/domainevent"
	"context"
)

type OrderCreatedEventHandler struct {
	orderPersistence output.OrderPersistencePort
}

func (p OrderCreatedEventHandler) Handle(ctx context.Context, event domainevent.Event[events.OrderCreatedDomainEvent]) error {
	//fmt.Println(event.GetPayload().Status)
	return nil
}
