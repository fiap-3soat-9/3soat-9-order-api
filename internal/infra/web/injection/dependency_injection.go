package injection

import (
	"3soat-9-order-api/config"
	"3soat-9-order-api/internal/domain/events"
	"3soat-9-order-api/internal/domain/events/subscriber"
	"3soat-9-order-api/internal/infra/database"
	consumer2 "3soat-9-order-api/internal/infra/messaging/consumer"
	"3soat-9-order-api/internal/infra/web/api/controller"
	order "3soat-9-order-api/internal/infra/web/api/rest"
	"3soat-9-order-api/internal/infra/web/api/swagger"
	"3soat-9-order-api/internal/usecase/command"
	"3soat-9-order-api/pkg/domainevent"
	"3soat-9-order-api/pkg/publisher"
	"context"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/logger"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/messaging/message"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/messaging/sqs/consumer"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/sql"
	"gorm.io/gorm"
)

type DependencyInjection struct {
	OrderApi *order.Api
	Swagger  *swagger.Swagger
}

func NewDependencyInjection() DependencyInjection {

	readWriteDB, readOnlyDB := sql.GetClient("readWrite"), sql.GetClient("readOnly")

	orderUseCaseController := controller.GetOrderUseCaseController(readWriteDB, readOnlyDB)

	RegisterDomainEvents(readWriteDB, readOnlyDB)
	RegisterConsumers(readWriteDB, readOnlyDB)

	return DependencyInjection{
		OrderApi: &order.Api{
			CreateOrderUseCase: orderUseCaseController.CreateOrderUseCase,
			ListOrderUseCase:   orderUseCaseController.ListOrderUseCase,
		},
		Swagger: &swagger.Swagger{},
	}
}

func RegisterDomainEvents(readWriteDB, readOnlyDB *gorm.DB) {
	dispatcher := domainevent.NewEventDispatcher[events.OrderCreatedDomainEvent]()
	dispatcher.Register(
		"order.created",
		subscriber.NewOrderCreatedEventHandler(
			database.GetOrderPersistenceGateway(readWriteDB, readOnlyDB, logger.Get()),
			publisher.NewPublisher[message.Message[command.CreatePaymentCommand]]()))

}

func RegisterConsumers(readWriteDB, readOnlyDB *gorm.DB) {

	orderPersistence := database.GetOrderPersistenceGateway(readWriteDB, readOnlyDB, logger.Get())
	paymentPersistence := database.GetPaymentRequestPersistence(readWriteDB, readOnlyDB, logger.Get())
	paymentCreatedConsumer := consumer2.NewPaymentCreatedConsumer(logger.Get(), orderPersistence, paymentPersistence)
	paymentUpdatedConsumer := consumer2.NewPaymentUpdatedConsumer(logger.Get(), orderPersistence, paymentPersistence)

	queues := config.GetConfig().Queues

	go consumer.NewConsumerWithErrDecorator[consumer2.PaymentUpdatedMessage](
		context.TODO(),
		queues.PaymentUpdated,
		paymentUpdatedConsumer.Handler,
		paymentUpdatedConsumer.HandlerError,
	)

	go consumer.NewConsumerWithErrDecorator[consumer2.PaymentCreatedMessage](
		context.TODO(),
		queues.PaymentCreated,
		paymentCreatedConsumer.Handler,
		paymentCreatedConsumer.HandlerError,
	)

}
