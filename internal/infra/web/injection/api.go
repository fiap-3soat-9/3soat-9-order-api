package injection

import (
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/httpserver"
)

func GetAllApis(injection DependencyInjection) []httpserver.Controller {
	return []httpserver.Controller{
		injection.OrderApi,
		injection.Swagger,
	}
}
