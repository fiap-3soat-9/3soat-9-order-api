package presenter

import (
	response2 "3soat-9-order-api/internal/infra/web/api/rest/response"
	"3soat-9-order-api/internal/usecase/result"
	"sort"
)

func OrderResponseFromResult(result result.CreateOrderResult) response2.OrderResponse {
	return response2.OrderResponse{
		Number:      result.Number,
		Amount:      result.Amount,
		PaymentData: result.PaymentData,
	}
}

func ListOrderResponseFromResult(resultOrders []result.ListOrderResult) []response2.ListOrderResponse {
	var ordersResponse []response2.ListOrderResponse
	orderSorted(resultOrders)
	for _, order := range resultOrders {
		var productsResponse []response2.ListOrderProducts
		for _, product := range order.Products {
			var ingredientsResponse []response2.ListOrderProductsIngredients
			for _, ingredient := range product.Ingredients {
				ingredientsResponse = append(ingredientsResponse, response2.ListOrderProductsIngredients{
					Name:     ingredient.Name,
					Amount:   ingredient.Amount,
					Quantity: ingredient.Quantity,
				})
			}
			productsResponse = append(productsResponse, response2.ListOrderProducts{
				Name:        product.Name,
				Number:      product.Number,
				Amount:      product.Amount,
				Quantity:    product.Quantity,
				Ingredients: ingredientsResponse,
			})
		}

		ordersResponse = append(ordersResponse, response2.ListOrderResponse{
			OrderNumber: order.OrderNumber,
			Status:      order.Status,
			Amount:      order.Amount,
			CustomerId:  order.CustomerId,
			CreatedAt:   order.CreatedAt,
			Products:    productsResponse,
		})
	}
	return ordersResponse
}

func GetOrderResponseFromResult(order result.ListOrderResult) response2.ListOrderResponse {

	var productsResponse []response2.ListOrderProducts
	for _, product := range order.Products {
		var ingredientsResponse []response2.ListOrderProductsIngredients
		for _, ingredient := range product.Ingredients {
			ingredientsResponse = append(ingredientsResponse, response2.ListOrderProductsIngredients{
				Name:     ingredient.Name,
				Amount:   ingredient.Amount,
				Quantity: ingredient.Quantity,
			})
		}
		productsResponse = append(productsResponse, response2.ListOrderProducts{
			Name:        product.Name,
			Number:      product.Number,
			Amount:      product.Amount,
			Quantity:    product.Quantity,
			Ingredients: ingredientsResponse,
		})
	}

	return response2.ListOrderResponse{
		OrderNumber: order.OrderNumber,
		Status:      order.Status,
		Amount:      order.Amount,
		CustomerId:  order.CustomerId,
		CreatedAt:   order.CreatedAt,
		Products:    productsResponse,
	}
}
func orderSorted(orders []result.ListOrderResult) {
	sort.Slice(orders, func(i, j int) bool {
		o, err := orders[i].GetStatus().Order()
		if err != nil {
			return false
		}
		o2, err := orders[j].GetStatus().Order()
		if err != nil {
			return false
		}
		return o < o2
	})
}
