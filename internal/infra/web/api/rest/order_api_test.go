package order

import (
	"3soat-9-order-api/internal/usecase/result"
	mocks "3soat-9-order-api/tests/mocks/port/input"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"
	"testing"
	"time"
)

func TestOrderApi(t *testing.T) {
	t.Run(`should create order`, func(t *testing.T) {
		createOrderUseCaseMock := mocks.NewCreateOrderPort(t)
		listOrderUseCaseMock := mocks.NewListOrderPort(t)

		orderApi := Api{
			CreateOrderUseCase: createOrderUseCaseMock,
			ListOrderUseCase:   listOrderUseCaseMock,
		}

		orderCreated := &result.CreateOrderResult{
			Number:      1,
			Amount:      1000,
			PaymentData: "data",
		}

		requestBody := `{
			"customerDocument": "21709085606",
			"products": [
				{
					"number": 1,
					"type": "default",
					"quantity": 2
				},
				{
					"number": 4,
					"type": "default",
					"quantity": 2
				}
			]
		}`

		createOrderUseCaseMock.On("AddOrder", mock.Anything, mock.Anything).Return(orderCreated, nil)

		echoServer := echo.New()
		testUrl := "http://localhost:8080/v1/orders"
		req, _ := http.NewRequest(http.MethodPost, testUrl, strings.NewReader(requestBody))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)

		response := orderApi.AddOrder(echoContext)

		createOrderUseCaseMock.AssertExpectations(t)
		createOrderUseCaseMock.AssertCalled(t, "AddOrder", mock.Anything, mock.Anything)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusOK, echoContext.Response().Status)
	})

	t.Run(`should return 400 when create product when payload is invalid`, func(t *testing.T) {
		createOrderUseCaseMock := mocks.NewCreateOrderPort(t)
		listOrderUseCaseMock := mocks.NewListOrderPort(t)

		orderApi := Api{
			CreateOrderUseCase: createOrderUseCaseMock,
			ListOrderUseCase:   listOrderUseCaseMock,
		}

		requestBody := `{
			"customerDocument": 1,
			"products": [
				{
					"number": 1,
					"type": "default",
					"quantity": 2
				},
				{
					"number": 4,
					"type": "default",
					"quantity": 2
				}
			]
		}`

		echoServer := echo.New()
		testUrl := "http://localhost:8080/v1/orders"
		req, _ := http.NewRequest(http.MethodPost, testUrl, strings.NewReader(requestBody))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)

		response := orderApi.AddOrder(echoContext)

		createOrderUseCaseMock.AssertExpectations(t)
		createOrderUseCaseMock.AssertNotCalled(t, "AddOrder", mock.Anything, mock.Anything)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusBadRequest, echoContext.Response().Status)
	})

	t.Run(`should return error when try to create order`, func(t *testing.T) {
		createOrderUseCaseMock := mocks.NewCreateOrderPort(t)
		listOrderUseCaseMock := mocks.NewListOrderPort(t)

		orderApi := Api{
			CreateOrderUseCase: createOrderUseCaseMock,
			ListOrderUseCase:   listOrderUseCaseMock,
		}

		requestBody := `{
			"customerDocument": "21709085606",
			"products": [
				{
					"number": 1,
					"type": "default",
					"quantity": 2
				},
				{
					"number": 4,
					"type": "default",
					"quantity": 2
				}
			]
		}`

		createOrderUseCaseMock.On("AddOrder", mock.Anything, mock.Anything).Return(nil, errors.New("error"))

		echoServer := echo.New()
		testUrl := "http://localhost:8080/v1/orders"
		req, _ := http.NewRequest(http.MethodPost, testUrl, strings.NewReader(requestBody))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)

		response := orderApi.AddOrder(echoContext)

		createOrderUseCaseMock.AssertExpectations(t)
		createOrderUseCaseMock.AssertCalled(t, "AddOrder", mock.Anything, mock.Anything)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusInternalServerError, echoContext.Response().Status)
	})

	t.Run(`should find order by number`, func(t *testing.T) {
		createOrderUseCaseMock := mocks.NewCreateOrderPort(t)
		listOrderUseCaseMock := mocks.NewListOrderPort(t)

		orderApi := Api{
			CreateOrderUseCase: createOrderUseCaseMock,
			ListOrderUseCase:   listOrderUseCaseMock,
		}

		orderResponse := result.ListOrderResult{
			OrderNumber: 1,
			Status:      "created",
			Amount:      1000,
			CustomerId:  "12817051017",
			CreatedAt:   time.Now(),
			Products: []result.OrderProductResult{
				{
					Name:     "product",
					Number:   1,
					Amount:   1000,
					Quantity: 1,
					Ingredients: []result.ProductIngredientsResult{
						{
							Number:   1,
							Name:     "ingredient",
							Amount:   1000,
							Quantity: 1,
						},
					},
				},
			},
		}

		id := 1

		listOrderUseCaseMock.On("FindByNumber", mock.Anything, id).Return(&orderResponse, nil)

		echoServer := echo.New()
		testUrl := fmt.Sprintf("http://localhost:8080/v1/orders/%d", id)
		req, _ := http.NewRequest(http.MethodGet, testUrl, strings.NewReader(``))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)
		echoContext.SetParamNames("number")
		echoContext.SetParamValues(strconv.Itoa(id))

		response := orderApi.GetOrderByNumber(echoContext)

		listOrderUseCaseMock.AssertExpectations(t)
		listOrderUseCaseMock.AssertCalled(t, "FindByNumber", mock.Anything, id)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusOK, echoContext.Response().Status)
	})

	t.Run(`should find order by number with error`, func(t *testing.T) {
		createOrderUseCaseMock := mocks.NewCreateOrderPort(t)
		listOrderUseCaseMock := mocks.NewListOrderPort(t)

		orderApi := Api{
			CreateOrderUseCase: createOrderUseCaseMock,
			ListOrderUseCase:   listOrderUseCaseMock,
		}

		id := 1

		listOrderUseCaseMock.On("FindByNumber", mock.Anything, id).Return(nil, errors.New("error"))

		echoServer := echo.New()
		testUrl := fmt.Sprintf("http://localhost:8080/v1/orders/%d", id)
		req, _ := http.NewRequest(http.MethodGet, testUrl, strings.NewReader(``))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)
		echoContext.SetParamNames("number")
		echoContext.SetParamValues(strconv.Itoa(id))

		response := orderApi.GetOrderByNumber(echoContext)

		listOrderUseCaseMock.AssertExpectations(t)
		listOrderUseCaseMock.AssertCalled(t, "FindByNumber", mock.Anything, id)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusInternalServerError, echoContext.Response().Status)
	})

	t.Run(`should return error when find order by invalid number`, func(t *testing.T) {
		createOrderUseCaseMock := mocks.NewCreateOrderPort(t)
		listOrderUseCaseMock := mocks.NewListOrderPort(t)

		orderApi := Api{
			CreateOrderUseCase: createOrderUseCaseMock,
			ListOrderUseCase:   listOrderUseCaseMock,
		}

		id := uuid.NewString()

		echoServer := echo.New()
		testUrl := fmt.Sprintf("http://localhost:8080/v1/orders/%s", id)
		req, _ := http.NewRequest(http.MethodGet, testUrl, strings.NewReader(``))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)
		echoContext.SetParamNames("number")
		echoContext.SetParamValues(id)

		response := orderApi.GetOrderByNumber(echoContext)

		listOrderUseCaseMock.AssertExpectations(t)
		listOrderUseCaseMock.AssertNotCalled(t, "FindByNumber", mock.Anything, id)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusBadRequest, echoContext.Response().Status)
	})

	t.Run(`should return error when find order by number without pass any value`, func(t *testing.T) {
		createOrderUseCaseMock := mocks.NewCreateOrderPort(t)
		listOrderUseCaseMock := mocks.NewListOrderPort(t)

		orderApi := Api{
			CreateOrderUseCase: createOrderUseCaseMock,
			ListOrderUseCase:   listOrderUseCaseMock,
		}

		echoServer := echo.New()
		testUrl := fmt.Sprintf("http://localhost:8080/v1/orders/%s", "")
		req, _ := http.NewRequest(http.MethodGet, testUrl, strings.NewReader(``))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)
		echoContext.SetParamNames("number")
		echoContext.SetParamValues("")

		response := orderApi.GetOrderByNumber(echoContext)

		listOrderUseCaseMock.AssertExpectations(t)
		listOrderUseCaseMock.AssertNotCalled(t, "FindByNumber", mock.Anything, mock.Anything)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusBadRequest, echoContext.Response().Status)
	})

	t.Run(`should find all orders`, func(t *testing.T) {
		createOrderUseCaseMock := mocks.NewCreateOrderPort(t)
		listOrderUseCaseMock := mocks.NewListOrderPort(t)

		orderApi := Api{
			CreateOrderUseCase: createOrderUseCaseMock,
			ListOrderUseCase:   listOrderUseCaseMock,
		}

		orderResponse := result.ListOrderResult{
			OrderNumber: 1,
			Status:      "CREATED",
			Amount:      1000,
			CustomerId:  "12817051017",
			CreatedAt:   time.Now(),
			Products: []result.OrderProductResult{
				{
					Name:     "product",
					Number:   1,
					Amount:   1000,
					Quantity: 1,
					Ingredients: []result.ProductIngredientsResult{
						{
							Number:   1,
							Name:     "ingredient",
							Amount:   1000,
							Quantity: 1,
						},
					},
				},
			},
		}

		orderResponse2 := result.ListOrderResult{
			OrderNumber: 1,
			Status:      "PAID",
			Amount:      1000,
			CustomerId:  "12817051017",
			CreatedAt:   time.Now(),
			Products: []result.OrderProductResult{
				{
					Name:     "product",
					Number:   1,
					Amount:   1000,
					Quantity: 1,
					Ingredients: []result.ProductIngredientsResult{
						{
							Number:   1,
							Name:     "ingredient",
							Amount:   1000,
							Quantity: 1,
						},
					},
				},
			},
		}

		listOrderUseCaseMock.On("FindAllOrders", mock.Anything).Return(
			[]result.ListOrderResult{orderResponse, orderResponse2},
			nil,
		)

		echoServer := echo.New()
		testUrl := "http://localhost:8080/v1/orders"
		req, _ := http.NewRequest(http.MethodGet, testUrl, strings.NewReader(``))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)

		response := orderApi.GetOrders(echoContext)

		listOrderUseCaseMock.AssertExpectations(t)
		listOrderUseCaseMock.AssertCalled(t, "FindAllOrders", mock.Anything)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusOK, echoContext.Response().Status)
	})

	t.Run(`should return error when try to find all orders`, func(t *testing.T) {
		createOrderUseCaseMock := mocks.NewCreateOrderPort(t)
		listOrderUseCaseMock := mocks.NewListOrderPort(t)

		orderApi := Api{
			CreateOrderUseCase: createOrderUseCaseMock,
			ListOrderUseCase:   listOrderUseCaseMock,
		}

		listOrderUseCaseMock.On("FindAllOrders", mock.Anything).Return(nil, errors.New("error"))

		echoServer := echo.New()
		testUrl := "http://localhost:8080/v1/orders"
		req, _ := http.NewRequest(http.MethodGet, testUrl, strings.NewReader(``))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)

		response := orderApi.GetOrders(echoContext)

		listOrderUseCaseMock.AssertExpectations(t)
		listOrderUseCaseMock.AssertCalled(t, "FindAllOrders", mock.Anything)
		assert.Nil(t, response)
		assert.Equal(t, http.StatusInternalServerError, echoContext.Response().Status)
	})

	t.Run(`should find orders by category`, func(t *testing.T) {
		createOrderUseCaseMock := mocks.NewCreateOrderPort(t)
		listOrderUseCaseMock := mocks.NewListOrderPort(t)

		orderApi := Api{
			CreateOrderUseCase: createOrderUseCaseMock,
			ListOrderUseCase:   listOrderUseCaseMock,
		}

		orderResponse := result.ListOrderResult{
			OrderNumber: 1,
			Status:      "created",
			Amount:      1000,
			CustomerId:  "12817051017",
			CreatedAt:   time.Now(),
			Products: []result.OrderProductResult{
				{
					Name:     "product",
					Number:   1,
					Amount:   1000,
					Quantity: 1,
					Ingredients: []result.ProductIngredientsResult{
						{
							Number:   1,
							Name:     "ingredient",
							Amount:   1000,
							Quantity: 1,
						},
					},
				},
			},
		}

		listOrderUseCaseMock.On("FindByStatus", mock.Anything, "created").Return([]result.ListOrderResult{orderResponse}, nil)

		echoServer := echo.New()
		testUrl := "http://localhost:8080/v1/orders?status=created"
		req, _ := http.NewRequest(http.MethodGet, testUrl, strings.NewReader(``))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)

		response := orderApi.GetOrders(echoContext)

		listOrderUseCaseMock.AssertExpectations(t)
		listOrderUseCaseMock.AssertCalled(t, "FindByStatus", mock.Anything, "created")
		assert.Nil(t, response)
		assert.Equal(t, http.StatusOK, echoContext.Response().Status)
	})

	t.Run(`should return error when try to find orders by category`, func(t *testing.T) {
		createOrderUseCaseMock := mocks.NewCreateOrderPort(t)
		listOrderUseCaseMock := mocks.NewListOrderPort(t)

		orderApi := Api{
			CreateOrderUseCase: createOrderUseCaseMock,
			ListOrderUseCase:   listOrderUseCaseMock,
		}

		listOrderUseCaseMock.On("FindByStatus", mock.Anything, "created").Return(nil, errors.New("error"))

		echoServer := echo.New()
		testUrl := "http://localhost:8080/v1/orders?status=created"
		req, _ := http.NewRequest(http.MethodGet, testUrl, strings.NewReader(``))
		req.Header.Add("X-B3-TraceId", "a818dadb-948a-4b58-80d2-d8afed7078bc")
		req.Header.Add("content-type", "application/json")

		rec := httptest.NewRecorder()
		echoContext := echoServer.NewContext(req, rec)

		response := orderApi.GetOrders(echoContext)

		listOrderUseCaseMock.AssertExpectations(t)
		listOrderUseCaseMock.AssertCalled(t, "FindByStatus", mock.Anything, "created")
		assert.Nil(t, response)
		assert.Equal(t, http.StatusInternalServerError, echoContext.Response().Status)
	})

}
