package controller

import (
	"3soat-9-order-api/internal/domain/events"
	"3soat-9-order-api/internal/domain/events/subscriber"
	"3soat-9-order-api/internal/infra/database"
	"3soat-9-order-api/internal/infra/http"
	"3soat-9-order-api/internal/port/input"
	"3soat-9-order-api/internal/usecase"
	"3soat-9-order-api/pkg/domainevent"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/logger"
	"gorm.io/gorm"
	"sync"
)

type OrderUseCaseController struct {
	CreateOrderUseCase input.CreateOrderPort
	ListOrderUseCase   input.ListOrderPort
	UpdateOrderUseCase input.UpdateOrderPort
}

var (
	orderUseCaseControllerInstance *OrderUseCaseController
	orderUseCaseControllerOnce     sync.Once
)

func GetOrderUseCaseController(readWriteDB, readOnlyDB *gorm.DB) *OrderUseCaseController {

	orderUseCaseControllerOnce.Do(func() {
		orderPersistence := database.GetOrderPersistenceGateway(readWriteDB, readOnlyDB, logger.Get())
		updateOrderUseCase := usecase.GetUpdateOrderUseCase(orderPersistence)

		createProcessPaymentUseCase := usecase.GetProcessPaymentUseCase(updateOrderUseCase, http.GetPaymentClient())
		//orderCreatedPublisher := publisher.NewPublisher[message.Message[command.CreatePaymentCommand]]()
		//orderCreatedSub := subscriber.NewOrderCreatedEventHandler(orderPersistence, orderCreatedPublisher)

		createOrderUseCase := usecase.GetCreateOrderUseCase(
			orderPersistence,
			http.GetProductClient(),
			createProcessPaymentUseCase,
			//*orderCreatedSub,
			OrderCreatedDispatcher(),
		)

		listOrderUseCase := usecase.GetListOrderUseCase(orderPersistence)

		orderUseCaseControllerInstance = &OrderUseCaseController{
			CreateOrderUseCase: createOrderUseCase,
			ListOrderUseCase:   listOrderUseCase,
			UpdateOrderUseCase: updateOrderUseCase,
		}
	})

	return orderUseCaseControllerInstance
}

func OrderCreatedDispatcher() *domainevent.Dispatcher[events.OrderCreatedDomainEvent] {
	dispatcher := domainevent.NewEventDispatcher[events.OrderCreatedDomainEvent]()
	dispatcher.Register("order.created", &subscriber.OrderCreatedEventHandler{})
	return dispatcher
}
