package mongo

//
//import (
//	domain2 "3soat-9-order-api/internal/domain"
//	"github.com/google/uuid"
//	"gorm.io/gorm"
//	"gorm.io/gorm/clause"
//)
//
//type IngredientTypeDBModel struct {
//	Name                    string                          `gorm:"primarykey"`
//	ConfigByProductCategory []IngredientTypeProductCategory `gorm:"foreignKey:IngredientType"`
//}
//
//type IngredientTypeProductCategory struct {
//	ID              uuid.UUID
//	IngredientType  string
//	Optional        bool
//	MaxQtd          int
//	ProductCategory string
//}
//
//func (i IngredientTypeDBModel) ToDomain() *domain2.IngredientType {
//	var configByProductCategory []domain2.IngredientTypeProductCategory
//	for _, config := range i.ConfigByProductCategory {
//		configByProductCategory = append(configByProductCategory, *config.ToDomain())
//	}
//	return &domain2.IngredientType{
//		Name:                    i.Name,
//		ConfigByProductCategory: configByProductCategory,
//	}
//}
//
//func (i IngredientTypeProductCategory) ToDomain() *domain2.IngredientTypeProductCategory {
//	return &domain2.IngredientTypeProductCategory{
//		IngredientType:  i.IngredientType,
//		Optional:        i.Optional,
//		MaxQtd:          i.MaxQtd,
//		ProductCategory: i.ProductCategory,
//	}
//}
//
//func FromIngredientTypeDomain(ingredientType domain2.IngredientType) IngredientTypeDBModel {
//	var configByProductCategory []IngredientTypeProductCategory
//
//	for _, config := range ingredientType.ConfigByProductCategory {
//		configByProductCategory = append(configByProductCategory, IngredientTypeProductCategory{
//			ID:              config.ID,
//			IngredientType:  config.IngredientType,
//			Optional:        config.Optional,
//			MaxQtd:          config.MaxQtd,
//			ProductCategory: config.ProductCategory,
//		})
//	}
//
//	return IngredientTypeDBModel{
//		Name:                    ingredientType.Name,
//		ConfigByProductCategory: configByProductCategory,
//	}
//}
//
//func (i IngredientTypeDBModel) BeforeCreate(tx *gorm.DB) (err error) {
//	var cols []clause.Column
//	var colsNames []string
//	for _, field := range tx.Statement.Schema.PrimaryFields {
//		cols = append(cols, clause.Column{Name: field.DBName})
//		colsNames = append(colsNames, field.DBName)
//	}
//	tx.Statement.AddClause(clause.OnConflict{
//		Columns:   cols,
//		DoNothing: true,
//	})
//	return nil
//}
//
//func (i IngredientTypeProductCategory) BeforeCreate(tx *gorm.DB) (err error) {
//	var cols []clause.Column
//	var colsNames []string
//	for _, field := range tx.Statement.Schema.PrimaryFields {
//		cols = append(cols, clause.Column{Name: field.DBName})
//		colsNames = append(colsNames, field.DBName)
//	}
//	tx.Statement.AddClause(clause.OnConflict{
//		Columns:   cols,
//		DoNothing: true,
//	})
//	return nil
//}
