package mongo

//
//import (
//	"3soat-9-order-api/internal/domain"
//	"github.com/google/uuid"
//	"gorm.io/gorm"
//	"gorm.io/gorm/clause"
//)
//
//type IngredientDBModel struct {
//	ID             uuid.UUID
//	Number         int    `gorm:"autoIncrement:true;unique"`
//	Name           string `gorm:"unique"`
//	Amount         int
//	Type           string
//	IngredientType IngredientTypeDBModel `gorm:"foreignKey:Type;references:Name"`
//}
//
//func (i IngredientDBModel) ToDomain() *domain.Ingredient {
//	return &domain.Ingredient{
//		ID:     i.ID,
//		Number: i.Number,
//		Name:   i.Name,
//		Amount: i.Amount,
//		Type:   *i.IngredientType.ToDomain(),
//	}
//}
//
//func (i IngredientDBModel) BeforeCreate(tx *gorm.DB) (err error) {
//	var cols []clause.Column
//	var colsNames []string
//	for _, field := range tx.Statement.Schema.PrimaryFields {
//		cols = append(cols, clause.Column{Name: field.DBName})
//		colsNames = append(colsNames, field.DBName)
//	}
//	tx.Statement.AddClause(clause.OnConflict{
//		Columns:   cols,
//		DoNothing: true,
//	})
//	return nil
//}
