package model

//
//import (
//	"3soat-9-order-api/internal/domain"
//	"3soat-9-order-api/internal/infra/database/mongo"
//	"github.com/google/uuid"
//	"gorm.io/gorm"
//	"gorm.io/gorm/clause"
//	"time"
//)
//
//type ProductDBModel struct {
//	ID              uuid.UUID
//	Number          int    `gorm:"autoIncrement:true;unique"`
//	Name            string `gorm:"unique"`
//	Amount          int
//	Description     string
//	Category        string
//	ProductCategory domain.ProductCategory `gorm:"foreignKey:Category;references:Name"`
//	Menu            bool
//	ImgPath         string
//	CreatedAt       time.Time
//	UpdatedAt       time.Time
//	Ingredients     []ProductIngredientDBModel `gorm:"foreignKey:ProductId"`
//	Active          bool
//}
//
//type ProductIngredientDBModel struct {
//	ID           uuid.UUID
//	ProductId    uuid.UUID
//	IngredientId uuid.UUID
//	Ingredient   mongo.IngredientDBModel
//	Quantity     int
//	Amount       int
//}
//
//func (pi ProductIngredientDBModel) toProductIngredientDomain() *domain.ProductIngredient {
//	return &domain.ProductIngredient{
//		ID:        pi.ID,
//		ProductId: pi.ProductId,
//		Ingredient: domain.Ingredient{
//			ID:     pi.Ingredient.ID,
//			Number: pi.Ingredient.Number,
//			Name:   pi.Ingredient.Name,
//			Amount: pi.Ingredient.Amount,
//			Type:   *pi.Ingredient.IngredientType.ToDomain(),
//		},
//		Quantity: pi.Quantity,
//	}
//}
//
//func (p ProductDBModel) ToDomain() *domain.Product {
//	var ingredients []domain.ProductIngredient
//
//	for _, ingredient := range p.Ingredients {
//		ingredients = append(ingredients, *ingredient.toProductIngredientDomain()))
//	}
//
//	return &domain.Product{
//		ID:          p.ID,
//		Number:      p.Number,
//		Name:        p.Name,
//		Amount:      p.Amount,
//		Description: p.Description,
//		Category: domain.ProductCategory{
//			Name:         p.ProductCategory.Name,
//			AcceptCustom: p.ProductCategory.AcceptCustom,
//		},
//		Menu:        p.Menu,
//		ImgPath:     p.ImgPath,
//		CreatedAt:   p.CreatedAt,
//		UpdatedAt:   p.UpdatedAt,
//		Ingredients: ingredients,
//		Active:      p.Active,
//	}
//}
//
//func ProductFromDomain(product *domain.Product) ProductDBModel {
//	var ingredients []ProductIngredientDBModel
//	for _, ingredient := range product.Ingredients {
//		ingredients = append(ingredients, ProductIngredientFromDomain(ingredient))
//	}
//	return ProductDBModel{
//		ID:          product.ID,
//		Number:      product.Number,
//		Name:        product.Name,
//		Amount:      product.Amount,
//		Description: product.Description,
//		ProductCategory: domain.ProductCategory{
//			Name:         product.Category.Name,
//			AcceptCustom: product.Category.AcceptCustom,
//		},
//		Menu:        product.Menu,
//		ImgPath:     product.ImgPath,
//		CreatedAt:   product.CreatedAt,
//		UpdatedAt:   product.UpdatedAt,
//		Ingredients: ingredients,
//	}
//}
//
//func (pi ProductIngredientDBModel) ToDomain() domain.ProductIngredient {
//	return domain.ProductIngredient{
//		ID:        pi.ID,
//		ProductId: pi.ProductId,
//		Ingredient: domain.Ingredient{
//			ID:     pi.Ingredient.ID,
//			Number: pi.Ingredient.Number,
//			Name:   pi.Ingredient.Name,
//			Amount: pi.Ingredient.Amount,
//			Type:   *pi.Ingredient.IngredientType.ToDomain(),
//		},
//		Quantity: pi.Quantity,
//		Amount:   pi.Amount,
//	}
//}
//
//func ProductIngredientFromDomain(productIngredient domain.ProductIngredient) ProductIngredientDBModel {
//	return ProductIngredientDBModel{
//		ID:        productIngredient.ID,
//		ProductId: productIngredient.ProductId,
//		Ingredient: domain.Ingredient{
//			ID:             productIngredient.Ingredient.ID,
//			Number:         productIngredient.Ingredient.Number,
//			Name:           productIngredient.Ingredient.Name,
//			Amount:         productIngredient.Ingredient.Amount,
//			IngredientType: FromIngredientTypeDomain(productIngredient.Ingredient.Type),
//		},
//		Quantity: productIngredient.Quantity,
//		Amount:   productIngredient.Amount,
//	}
//}
//
//func (p Product) BeforeCreate(tx *gorm.DB) (err error) {
//	var cols []clause.Column
//	var colsNames []string
//	for _, field := range tx.Statement.Schema.PrimaryFields {
//		cols = append(cols, clause.Column{Name: field.DBName})
//		colsNames = append(colsNames, field.DBName)
//	}
//	tx.Statement.AddClause(clause.OnConflict{
//		Columns:   cols,
//		DoNothing: true,
//	})
//	return nil
//}
