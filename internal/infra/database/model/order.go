package model

import (
	"3soat-9-order-api/internal/domain"
	"3soat-9-order-api/internal/domain/valueobject"
	"github.com/google/uuid"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"time"
)

type Order struct {
	ID         uuid.UUID
	Number     int `gorm:"autoIncrement:true;unique"`
	CustomerId string
	Products   []OrderProduct `gorm:"foreignKey:OrderId"`
	CreatedAt  time.Time
	UpdatedAt  time.Time
	Status     string
	Amount     int
	History    []OrderHistory `gorm:"foreignKey:OrderId"`
	PaymentId  uuid.UUID
}

type OrderProduct struct {
	ID uuid.UUID
	//ProductId uuid.UUID
	OrderId  uuid.UUID `gorm:"foreignKey:OrderId"`
	Quantity int
	Amount   int
}

func (o Order) ToDomain() *domain.Order {
	var products []domain.OrderProduct
	for _, orderProduct := range o.Products {
		products = append(products, *orderProduct.ToDomain())
	}
	var orderHistory []domain.OrderHistory
	for _, history := range o.History {
		orderHistory = append(orderHistory, history.ToDomain())
	}
	return &domain.Order{
		Id:         o.ID,
		Number:     o.Number,
		CustomerId: o.CustomerId,
		Products:   products,
		CreatedAt:  o.CreatedAt,
		UpdatedAt:  o.UpdatedAt,
		Status:     valueobject.OrderStatus(o.Status),
		Amount:     o.Amount,
		History:    orderHistory,
		PaymentId:  o.PaymentId,
	}
}

func (o OrderProduct) ToDomain() *domain.OrderProduct {
	return &domain.OrderProduct{
		Id:       o.ID,
		OrderId:  o.OrderId,
		Quantity: o.Quantity,
		Amount:   o.Amount,
	}
}

func FromDomain(order domain.Order) *Order {
	var orderProducts []OrderProduct
	for _, product := range order.Products {
		orderProducts = append(orderProducts, OrderProductFromDomain(product))
	}

	var orderHistories []OrderHistory
	for _, history := range order.History {
		orderHistories = append(orderHistories, OrderHistoryFromDomain(history))
	}
	return &Order{
		ID:         order.Id,
		Number:     order.Number,
		CustomerId: order.CustomerId,
		Products:   orderProducts,
		CreatedAt:  order.CreatedAt,
		UpdatedAt:  order.UpdatedAt,
		Status:     string(order.Status),
		Amount:     order.Amount,
		PaymentId:  order.PaymentId,
		History:    orderHistories,
	}
}

func OrderProductFromDomain(orderProduct domain.OrderProduct) OrderProduct {
	return OrderProduct{
		ID:       orderProduct.Id,
		OrderId:  orderProduct.OrderId,
		Quantity: orderProduct.Quantity,
		Amount:   orderProduct.Amount,
	}
}

func (o Order) BeforeCreate(tx *gorm.DB) (err error) {
	var cols []clause.Column
	var colsNames []string
	for _, field := range tx.Statement.Schema.PrimaryFields {
		cols = append(cols, clause.Column{Name: field.DBName})
		colsNames = append(colsNames, field.DBName)
	}
	tx.Statement.AddClause(clause.OnConflict{
		Columns:   cols,
		DoNothing: true,
	})
	return nil
}

func (o OrderProduct) BeforeCreate(tx *gorm.DB) (err error) {
	var cols []clause.Column
	var colsNames []string
	for _, field := range tx.Statement.Schema.PrimaryFields {
		cols = append(cols, clause.Column{Name: field.DBName})
		colsNames = append(colsNames, field.DBName)
	}
	tx.Statement.AddClause(clause.OnConflict{
		Columns:   cols,
		DoNothing: true,
	})
	return nil
}
