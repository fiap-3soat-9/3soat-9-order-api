package model

import "time"

type PaymentRequests struct {
	ID          string
	ReferenceID string
	OrderID     string
	Amount      int64
	Status      string
	Attempts    int
	CreatedAt   time.Time
	UpdatedAt   time.Time
}
