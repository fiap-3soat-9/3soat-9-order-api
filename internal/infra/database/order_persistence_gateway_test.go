package database

import (
	"3soat-9-order-api/internal/domain"
	"3soat-9-order-api/tests/mocks/db"
	"context"
	"errors"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/google/uuid"
	"github.com/rs/zerolog"
	"github.com/stretchr/testify/assert"
	"gitlab.com/fiap-3soat-9/go-pkg-soat3/logger"
	"gorm.io/gorm"
	"testing"
	"time"
)

func TestOrderRepository(t *testing.T) {
	sqlDB, gormDB, mock := db.GetMockDB(t)
	defer sqlDB.Close()

	t.Run(`should find all orders`, func(t *testing.T) {
		orderId := uuid.New()
		orderRows, orderHistoryRows, orderProductRows := generateRows(orderId)

		mock.ExpectQuery("^SELECT \\* FROM \"order\"").
			WillReturnRows(orderRows)
		mock.ExpectQuery("^SELECT \\* FROM \"order_history\" WHERE \"order_history\"\\.\"order_id\" = .*").
			WillReturnRows(orderHistoryRows)
		mock.ExpectQuery("^SELECT \\* FROM \"order_product\" WHERE \"order_product\"\\.\"order_id\" = .*").
			WillReturnRows(orderProductRows)

		orderPersistenceInstance := GetOrderPersistenceGateway(gormDB, gormDB, logger.Get())
		orders, err := orderPersistenceInstance.FindAll(context.TODO())

		assert.Nil(t, err)
		assert.NotNil(t, orders)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should return error when try to find all orders`, func(t *testing.T) {
		mock.ExpectQuery("^SELECT \\* FROM \"order\"").
			WillReturnError(errors.New("error"))

		orderPersistenceInstance := OrderPersistenceGateway{
			readWriteClient: gormDB,
			readOnlyClient:  gormDB,
			logger:          zerolog.Nop(),
		}
		order, err := orderPersistenceInstance.FindAll(context.TODO())

		assert.NotNil(t, err)
		assert.Nil(t, order)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should find order by status`, func(t *testing.T) {
		orderId := uuid.New()
		orderRows, _, _ := generateRows(orderId)

		mock.ExpectQuery("^SELECT \\* FROM \"order\" WHERE status = .*").
			WillReturnRows(orderRows)

		orderPersistenceInstance := GetOrderPersistenceGateway(gormDB, gormDB, logger.Get())
		orders, err := orderPersistenceInstance.FindByStatus(context.TODO(), "created")

		assert.Nil(t, err)
		assert.NotNil(t, orders)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should find order by ID`, func(t *testing.T) {
		orderId := uuid.New()
		orderRows, _, _ := generateRows(orderId)

		mock.ExpectQuery("^SELECT \\* FROM \"order\" WHERE \"order\"\\.\"\\w+\" = .*").
			WillReturnRows(orderRows)

		orderPersistenceInstance := OrderPersistenceGateway{
			readWriteClient: gormDB,
			readOnlyClient:  gormDB,
			logger:          zerolog.Nop(),
		}
		order, err := orderPersistenceInstance.FindById(context.TODO(), uuid.New())

		assert.Nil(t, err)
		assert.NotNil(t, order)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should return error when try to find order by ID`, func(t *testing.T) {
		mock.ExpectQuery("^SELECT \\* FROM \"order\" WHERE \"order\"\\.\"\\w+\" = .*").
			WillReturnError(errors.New("error"))

		orderPersistenceInstance := OrderPersistenceGateway{
			readWriteClient: gormDB,
			readOnlyClient:  gormDB,
			logger:          zerolog.Nop(),
		}
		order, err := orderPersistenceInstance.FindById(context.TODO(), uuid.New())

		assert.NotNil(t, err)
		assert.Nil(t, order)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should return nil when try id not found`, func(t *testing.T) {
		mock.ExpectQuery("^SELECT \\* FROM \"order\" WHERE \"order\"\\.\"\\w+\" = .*").
			WillReturnError(gorm.ErrRecordNotFound)

		orderPersistenceInstance := OrderPersistenceGateway{
			readWriteClient: gormDB,
			readOnlyClient:  gormDB,
			logger:          zerolog.Nop(),
		}
		order, err := orderPersistenceInstance.FindById(context.TODO(), uuid.New())

		assert.Nil(t, err)
		assert.Nil(t, order)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should find order by number`, func(t *testing.T) {
		orderId := uuid.New()
		orderRows, orderHistoryRows, orderProductRows := generateRows(orderId)

		mock.ExpectQuery("^SELECT \\* FROM \"order\" WHERE number = .*$").
			WillReturnRows(orderRows)
		mock.ExpectQuery("^SELECT \\* FROM \"order_history\" WHERE \"order_history\"\\.\"order_id\" = .*").
			WillReturnRows(orderHistoryRows)
		mock.ExpectQuery("^SELECT \\* FROM \"order_product\" WHERE \"order_product\"\\.\"order_id\" = .*").
			WillReturnRows(orderProductRows)

		orderPersistenceInstance := OrderPersistenceGateway{
			readWriteClient: gormDB,
			readOnlyClient:  gormDB,
			logger:          zerolog.Nop(),
		}
		order, err := orderPersistenceInstance.FindByNumber(context.TODO(), 1)

		assert.Nil(t, err)
		assert.NotNil(t, order)
	})

	t.Run(`should return error when try to find order by number`, func(t *testing.T) {
		mock.ExpectQuery("^SELECT \\* FROM \"order\" WHERE number = .*$").
			WillReturnError(errors.New("error"))

		orderPersistenceInstance := OrderPersistenceGateway{
			readWriteClient: gormDB,
			readOnlyClient:  gormDB,
			logger:          zerolog.Nop(),
		}
		order, err := orderPersistenceInstance.FindByNumber(context.TODO(), 1)

		assert.NotNil(t, err)
		assert.Nil(t, order)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should return nil when number not found`, func(t *testing.T) {
		mock.ExpectQuery("^SELECT \\* FROM \"order\" WHERE number = .*$").
			WillReturnError(gorm.ErrRecordNotFound)

		orderPersistenceInstance := OrderPersistenceGateway{
			readWriteClient: gormDB,
			readOnlyClient:  gormDB,
			logger:          zerolog.Nop(),
		}
		order, err := orderPersistenceInstance.FindByNumber(context.TODO(), 1)

		assert.Nil(t, err)
		assert.Nil(t, order)

		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should create order`, func(t *testing.T) {
		mock.ExpectQuery("^INSERT INTO \"order\" .*$").
			WillReturnRows(sqlmock.NewRows([]string{"number"}).AddRow(1))
		mock.ExpectExec("^INSERT INTO \"order_product\" (.+)$").
			WillReturnResult(sqlmock.NewResult(1, 1))
		mock.ExpectExec("^INSERT INTO \"order_history\" (.+)$").
			WillReturnResult(sqlmock.NewResult(1, 1))

		orderPersistenceInstance := GetOrderPersistenceGateway(gormDB, gormDB, logger.Get())

		orderId := uuid.New()

		orderProduct := domain.OrderProduct{
			Id: uuid.New(),
			Product: domain.Product{
				Number:      1,
				Name:        "Product",
				Amount:      1000,
				Description: "desc",
				Category:    "category",
				ImgPath:     "path",
				Ingredients: []domain.ProductIngredient{
					{
						Number:   1,
						Name:     "ingredient",
						Amount:   1000,
						Quantity: 1,
					},
				},
				Active: false,
			},
			OrderId:  orderId,
			Quantity: 1,
			Amount:   1000,
		}

		err := orderPersistenceInstance.Create(context.TODO(), domain.Order{
			Id:         orderId,
			Number:     1,
			CustomerId: "customer",
			Products:   []domain.OrderProduct{orderProduct},
			CreatedAt:  time.Now(),
			UpdatedAt:  time.Now(),
			Status:     "created",
			Amount:     1000,
			History: []domain.OrderHistory{{
				Id:        uuid.New(),
				OrderId:   uuid.New(),
				Status:    "created",
				ChangeBy:  "USER",
				CreatedAt: time.Now(),
			}},
			PaymentId: uuid.New(),
		})
		assert.Nil(t, err)
		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should update order`, func(t *testing.T) {
		mock.ExpectExec("^UPDATE \"order\" .*$").
			WillReturnResult(sqlmock.NewResult(1, 1))
		mock.ExpectExec("^INSERT INTO \"order_product\" (.+)$").
			WillReturnResult(sqlmock.NewResult(1, 1))
		mock.ExpectExec("^INSERT INTO \"order_history\" (.+)$").
			WillReturnResult(sqlmock.NewResult(1, 1))

		orderPersistenceInstance := GetOrderPersistenceGateway(gormDB, gormDB, logger.Get())

		orderId := uuid.New()

		orderProduct := domain.OrderProduct{
			Id: uuid.New(),
			Product: domain.Product{
				Number:      1,
				Name:        "Product",
				Amount:      1000,
				Description: "desc",
				Category:    "category",
				ImgPath:     "path",
				Ingredients: []domain.ProductIngredient{
					{
						Number:   1,
						Name:     "ingredient",
						Amount:   1000,
						Quantity: 1,
					},
				},
				Active: false,
			},
			OrderId:  orderId,
			Quantity: 1,
			Amount:   1000,
		}

		err := orderPersistenceInstance.Update(context.TODO(), domain.Order{
			Id:         orderId,
			Number:     1,
			CustomerId: "customer",
			Products:   []domain.OrderProduct{orderProduct},
			CreatedAt:  time.Now(),
			UpdatedAt:  time.Now(),
			Status:     "created",
			Amount:     1000,
			History: []domain.OrderHistory{{
				Id:        uuid.New(),
				OrderId:   uuid.New(),
				Status:    "created",
				ChangeBy:  "USER",
				CreatedAt: time.Now(),
			}},
			PaymentId: uuid.New(),
		})
		assert.Nil(t, err)
		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})

	t.Run(`should update order status`, func(t *testing.T) {
		mock.ExpectExec("^UPDATE \"order\" .*$").
			WillReturnResult(sqlmock.NewResult(1, 1))

		orderPersistenceInstance := GetOrderPersistenceGateway(gormDB, gormDB, logger.Get())

		orderId := uuid.New()

		err := orderPersistenceInstance.UpdateStatus(context.TODO(), orderId.String(), "paid")
		assert.Nil(t, err)
		err = mock.ExpectationsWereMet()
		assert.Nil(t, err)
	})
}

func generateRows(orderId uuid.UUID) (*sqlmock.Rows, *sqlmock.Rows, *sqlmock.Rows) {
	orderRows := sqlmock.NewRows([]string{"id", "number", "customer_id", "created_at", "updated_at", "status", "amount", "payment_id"}).
		AddRow(orderId, 1, "customer_id", time.Now(), time.Now(), "status", 1000, uuid.New())
	orderHistoryRows := sqlmock.NewRows([]string{"id", "order_id", "status", "changed_by", "created_at"}).
		AddRow(uuid.New(), orderId, "created", "USER", time.Now())
	orderProductRows := sqlmock.NewRows([]string{"id", "order_id", "product_id", "quantity", "amount"}).
		AddRow(uuid.New(), orderId, uuid.New(), 1, 1000)
	return orderRows, orderHistoryRows, orderProductRows
}
