package database

import (
	"3soat-9-order-api/internal/infra/database/model"
	"context"
	"github.com/rs/zerolog"
	"gorm.io/gorm"
	"sync"
)

var (
	paymentRequestRepositoryInstance *PaymentRequestPersistence
	paymentRequestRepositoryOnce     sync.Once
)

type PaymentRequestPersistence struct {
	readWriteClient *gorm.DB
	readOnlyClient  *gorm.DB
	logger          zerolog.Logger
}

func (p PaymentRequestPersistence) Create(ctx context.Context, paymentRequestCommand model.PaymentRequests) (any, error) {
	err := p.readWriteClient.Create(&paymentRequestCommand).Error
	if err != nil {
		p.logger.Error().
			Ctx(ctx).
			Err(err).
			Str("payment id", paymentRequestCommand.ID).
			Msg("Failed to insert payment request")
		return nil, err
	}
	return paymentRequestCommand, nil
}

func (p PaymentRequestPersistence) FindByReference(ctx context.Context, reference string) (*model.PaymentRequests, error) {
	var orders model.PaymentRequests
	err := p.readOnlyClient.Where("reference_id = ?", reference).First(&orders).Error
	if err != nil {
		p.logger.Error().
			Ctx(ctx).
			Err(err).
			Str("reference", reference).
			Msg("Failed to find payment request by reference")
		return nil, err
	}

	return &orders, nil
}

func (p PaymentRequestPersistence) Update(ctx context.Context, paymentReference, status string) error {
	err := p.readWriteClient.
		Model(&model.PaymentRequests{}).
		Where("reference_id = ?", paymentReference).
		Update("status", status).
		Error
	if err != nil {
		p.logger.Error().
			Ctx(ctx).
			Err(err).
			Str("paymentId", paymentReference).
			Msg("Failed to update payment request")
		return err
	}
	return nil
}

func GetPaymentRequestPersistence(readWriteClient *gorm.DB, readOnlyClient *gorm.DB, logger zerolog.Logger) *PaymentRequestPersistence {
	paymentRequestRepositoryOnce.Do(func() {
		paymentRequestRepositoryInstance = &PaymentRequestPersistence{
			readWriteClient: readWriteClient,
			readOnlyClient:  readOnlyClient,
			logger:          logger,
		}
	})
	return paymentRequestRepositoryInstance

}
