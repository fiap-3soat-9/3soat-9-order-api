package usecase

import (
	"3soat-9-order-api/internal/domain"
	"3soat-9-order-api/internal/domain/valueobject"
	"3soat-9-order-api/internal/port/input"
	"3soat-9-order-api/internal/port/output"
	"3soat-9-order-api/internal/usecase/result"
	"context"
	"sync"
)

var (
	processPaymentUseCaseInstance *ProcessPaymentUseCase
	processPaymentUseCaseOnce     sync.Once
)

type ProcessPaymentUseCase struct {
	updateOrderUseCase  input.UpdateOrderPort
	createPaymentClient output.PaymentClientGatewayPort
}

func (p ProcessPaymentUseCase) PublishToProcessPayment(ctx context.Context, order domain.Order) (*result.PaymentCreatedResult, error) {
	//TODO implement me
	panic("implement me")
}

func (p ProcessPaymentUseCase) ProcessPayment(ctx context.Context, order domain.Order) (*result.PaymentCreatedResult, error) {
	paymentData, err := p.createPaymentClient.Create(ctx, order)
	if err != nil {
		return nil, err
	}

	err = p.updateOrderUseCase.Update(ctx, order.Id, valueobject.WaitPayment, &paymentData.PaymentId)
	if err != nil {
		return nil, err
	}
	return &result.PaymentCreatedResult{
		PaymentId:   paymentData.PaymentId,
		OrderId:     paymentData.OrderId,
		PaymentData: paymentData.PaymentData,
		CreatedAt:   paymentData.CreatedAt,
	}, nil
}

func GetProcessPaymentUseCase(
	updateOrderUseCase input.UpdateOrderPort,
	createPaymentClient output.PaymentClientGatewayPort,
) input.ProcessPaymentPort {
	processPaymentUseCaseOnce.Do(func() {
		processPaymentUseCaseInstance = &ProcessPaymentUseCase{
			updateOrderUseCase:  updateOrderUseCase,
			createPaymentClient: createPaymentClient,
		}
	})
	return processPaymentUseCaseInstance
}
