package result

import (
	"github.com/google/uuid"
	"time"
)

type PaymentCreatedResult struct {
	PaymentId   uuid.UUID
	OrderId     uuid.UUID
	PaymentData string
	CreatedAt   time.Time
}
