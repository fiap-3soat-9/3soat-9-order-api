package result

import (
	"3soat-9-order-api/internal/domain"
)

type FindProductResult struct {
	Name        string
	Number      int
	Amount      int
	Description string
	Category    string
	Menu        bool
	ImgPath     string
	Ingredients []ProductIngredientsResult
	Active      bool
}

type ProductIngredientsResult struct {
	Number   int
	Name     string
	Amount   int
	Quantity int
}

func FromProductIngredientDomain(productIngredient domain.ProductIngredient) ProductIngredientsResult {
	return ProductIngredientsResult{
		Number:   productIngredient.Number,
		Name:     productIngredient.Name,
		Amount:   productIngredient.Amount,
		Quantity: productIngredient.Quantity,
	}
}
