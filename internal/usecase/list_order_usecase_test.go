package usecase

import (
	"3soat-9-order-api/internal/domain"
	mocks "3soat-9-order-api/tests/mocks/port/output"
	"context"
	"errors"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"testing"
	"time"
)

func TestListOrderUseCase(t *testing.T) {

	t.Run(`should find all orders`, func(t *testing.T) {
		orderPersistenceMock := mocks.NewOrderPersistencePort(t)
		listOrderUseCase := GetListOrderUseCase(orderPersistenceMock)
		orderId := uuid.New()
		order := domain.Order{
			Id:         orderId,
			CustomerId: "Document",
			Products: []domain.OrderProduct{
				{
					Id: uuid.New(),
					Product: domain.Product{
						Number:      1,
						Name:        "Product",
						Amount:      1000,
						Description: "Product Description",
						Category:    "Category",
						ImgPath:     "https://imgpath.com",
						Ingredients: []domain.ProductIngredient{
							{
								Number:   1,
								Name:     "Product Ingredient",
								Amount:   1000,
								Quantity: 1,
							},
						},
						Active: true,
					},
					OrderId:  orderId,
					Quantity: 1,
					Amount:   1000,
				},
			},
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
			Status:    "WAITING_PAYMENT",
			Amount:    1000,
			PaymentId: uuid.New(),
		}

		orderPersistenceMock.On("FindAll", mock.Anything).Return([]domain.Order{order}, nil)

		orders, err := listOrderUseCase.FindAllOrders(context.TODO())

		assert.Nil(t, err)
		assert.Equal(t, len(orders), 1)
		orderPersistenceMock.AssertExpectations(t)
		orderPersistenceMock.AssertCalled(t, "FindAll", mock.Anything)
	})

	t.Run(`should return error when try to find all orders`, func(t *testing.T) {
		orderPersistenceMock := mocks.NewOrderPersistencePort(t)
		listOrderUseCase := ListOrderUseCase{
			orderPersistenceGateway: orderPersistenceMock,
		}

		orderPersistenceMock.On("FindAll", mock.Anything).Return(nil, errors.New("SOME_ERROR"))

		orders, err := listOrderUseCase.FindAllOrders(context.TODO())

		assert.NotNil(t, err)
		assert.Nil(t, orders)
		orderPersistenceMock.AssertExpectations(t)
		orderPersistenceMock.AssertCalled(t, "FindAll", mock.Anything)
	})

	t.Run(`should find order by status`, func(t *testing.T) {
		orderPersistenceMock := mocks.NewOrderPersistencePort(t)
		listOrderUseCase := ListOrderUseCase{
			orderPersistenceGateway: orderPersistenceMock,
		}
		orderId := uuid.New()
		order := domain.Order{
			Id:         orderId,
			CustomerId: "Document",
			Products: []domain.OrderProduct{
				{
					Id: uuid.New(),
					Product: domain.Product{
						Number:      1,
						Name:        "Product",
						Amount:      1000,
						Description: "Product Description",
						Category:    "Category",
						ImgPath:     "https://imgpath.com",
						Ingredients: []domain.ProductIngredient{
							{
								Number:   1,
								Name:     "Product Ingredient",
								Amount:   1000,
								Quantity: 1,
							},
						},
						Active: true,
					},
					OrderId:  orderId,
					Quantity: 1,
					Amount:   1000,
				},
			},
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
			Status:    "WAITING_PAYMENT",
			Amount:    1000,
			PaymentId: uuid.New(),
		}

		orderPersistenceMock.On("FindByStatus", mock.Anything, "WAITING_PAYMENT").Return([]domain.Order{order}, nil)

		orders, err := listOrderUseCase.FindByStatus(context.TODO(), "WAITING_PAYMENT")

		assert.Nil(t, err)
		assert.Equal(t, len(orders), 1)
		orderPersistenceMock.AssertExpectations(t)
		orderPersistenceMock.AssertCalled(t, "FindByStatus", mock.Anything, mock.Anything)
	})

	t.Run(`should return error when try to find orders by status`, func(t *testing.T) {
		orderPersistenceMock := mocks.NewOrderPersistencePort(t)
		listOrderUseCase := ListOrderUseCase{
			orderPersistenceGateway: orderPersistenceMock,
		}

		orderPersistenceMock.On("FindByStatus", mock.Anything, "WAITING_PAYMENT").Return(nil, errors.New("SOME_ERROR"))

		orders, err := listOrderUseCase.FindByStatus(context.TODO(), "WAITING_PAYMENT")

		assert.NotNil(t, err)
		assert.Nil(t, orders)
		orderPersistenceMock.AssertExpectations(t)
		orderPersistenceMock.AssertCalled(t, "FindByStatus", mock.Anything, mock.Anything)
	})

	t.Run(`should find order by number`, func(t *testing.T) {
		orderPersistenceMock := mocks.NewOrderPersistencePort(t)
		listOrderUseCase := ListOrderUseCase{
			orderPersistenceGateway: orderPersistenceMock,
		}
		orderId := uuid.New()
		order := domain.Order{
			Id:         orderId,
			CustomerId: "Document",
			Products: []domain.OrderProduct{
				{
					Id: uuid.New(),
					Product: domain.Product{
						Number:      1,
						Name:        "Product",
						Amount:      1000,
						Description: "Product Description",
						Category:    "Category",
						ImgPath:     "https://imgpath.com",
						Ingredients: []domain.ProductIngredient{
							{
								Number:   1,
								Name:     "Product Ingredient",
								Amount:   1000,
								Quantity: 1,
							},
						},
						Active: true,
					},
					OrderId:  orderId,
					Quantity: 1,
					Amount:   1000,
				},
			},
			CreatedAt: time.Now(),
			UpdatedAt: time.Now(),
			Status:    "WAITING_PAYMENT",
			Amount:    1000,
			PaymentId: uuid.New(),
		}

		orderPersistenceMock.On("FindByNumber", mock.Anything, 1).Return(&order, nil)

		orderResponse, err := listOrderUseCase.FindByNumber(context.TODO(), 1)

		assert.Nil(t, err)
		assert.NotNil(t, orderResponse)
		orderPersistenceMock.AssertExpectations(t)
		orderPersistenceMock.AssertCalled(t, "FindByNumber", mock.Anything, 1)
	})

	t.Run(`should return error when try to find orders by number`, func(t *testing.T) {
		orderPersistenceMock := mocks.NewOrderPersistencePort(t)
		listOrderUseCase := ListOrderUseCase{
			orderPersistenceGateway: orderPersistenceMock,
		}

		orderPersistenceMock.On("FindByNumber", mock.Anything, 1).Return(nil, errors.New("SOME_ERROR"))

		orders, err := listOrderUseCase.FindByNumber(context.TODO(), 1)

		assert.NotNil(t, err)
		assert.Nil(t, orders)
		orderPersistenceMock.AssertExpectations(t)
		orderPersistenceMock.AssertCalled(t, "FindByNumber", mock.Anything, mock.Anything)
	})
}
