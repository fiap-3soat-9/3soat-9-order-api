package usecase

import (
	"3soat-9-order-api/internal/domain"
	"3soat-9-order-api/internal/domain/valueobject"
	"3soat-9-order-api/internal/usecase/result"
	mocks2 "3soat-9-order-api/tests/mocks/port/input"
	mocks "3soat-9-order-api/tests/mocks/port/output"
	"context"
	"errors"
	"time"

	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestProcessPaymentUseCase(t *testing.T) {

	t.Run(`should process payment`, func(t *testing.T) {
		updateOrderUseCaseMock := mocks2.NewUpdateOrderPort(t)
		paymentClientGatewayMock := mocks.NewPaymentClientGatewayPort(t)
		processPaymentUseCase := GetProcessPaymentUseCase(updateOrderUseCaseMock, paymentClientGatewayMock)

		order := domain.Order{Id: uuid.New()}

		payment := result.PaymentCreatedResult{
			PaymentId:   uuid.New(),
			OrderId:     order.Id,
			PaymentData: "mocked",
			CreatedAt:   time.Now(),
		}

		paymentClientGatewayMock.On("Create", mock.Anything, mock.Anything).Return(&payment, nil)

		updateOrderUseCaseMock.On("Update", mock.Anything, payment.OrderId, valueobject.WaitPayment, &payment.PaymentId).Return(nil)

		paymentCreated, err := processPaymentUseCase.ProcessPayment(context.TODO(), order)

		assert.Nil(t, err)
		assert.Equal(t, paymentCreated.PaymentData, "mocked")

		paymentClientGatewayMock.AssertExpectations(t)
		paymentClientGatewayMock.AssertCalled(t, "Create", mock.Anything, mock.Anything)

		updateOrderUseCaseMock.AssertExpectations(t)
		updateOrderUseCaseMock.AssertCalled(t, "Update", mock.Anything, mock.Anything, mock.Anything, mock.Anything)
	})

	t.Run(`should return error when create payment failed`, func(t *testing.T) {
		updateOrderUseCaseMock := mocks2.NewUpdateOrderPort(t)
		paymentClientGatewayMock := mocks.NewPaymentClientGatewayPort(t)
		processPaymentUseCase := ProcessPaymentUseCase{
			updateOrderUseCase:  updateOrderUseCaseMock,
			createPaymentClient: paymentClientGatewayMock,
		}

		order := domain.Order{}

		paymentClientGatewayMock.On("Create", mock.Anything, mock.Anything).Return(nil, errors.New("SOME_ERROR"))

		paymentCreated, err := processPaymentUseCase.ProcessPayment(context.TODO(), order)

		assert.NotNil(t, err)
		assert.Nil(t, paymentCreated)

		paymentClientGatewayMock.AssertExpectations(t)
		paymentClientGatewayMock.AssertCalled(t, "Create", mock.Anything, mock.Anything)

		updateOrderUseCaseMock.AssertExpectations(t)
		updateOrderUseCaseMock.AssertNotCalled(t, "Update", mock.Anything, mock.Anything, mock.Anything)
	})

	t.Run(`should return error when update failed`, func(t *testing.T) {
		updateOrderUseCaseMock := mocks2.NewUpdateOrderPort(t)
		paymentClientGatewayMock := mocks.NewPaymentClientGatewayPort(t)
		processPaymentUseCase := ProcessPaymentUseCase{
			updateOrderUseCase:  updateOrderUseCaseMock,
			createPaymentClient: paymentClientGatewayMock,
		}

		order := domain.Order{Id: uuid.New()}

		payment := result.PaymentCreatedResult{
			PaymentId:   uuid.New(),
			OrderId:     order.Id,
			PaymentData: "mocked",
			CreatedAt:   time.Time{},
		}

		paymentClientGatewayMock.On("Create", mock.Anything, mock.Anything).Return(&payment, nil)
		updateOrderUseCaseMock.On("Update", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("SOME_ERROR"))

		paymentCreated, err := processPaymentUseCase.ProcessPayment(context.TODO(), order)

		assert.NotNil(t, err)
		assert.Nil(t, paymentCreated)

		paymentClientGatewayMock.AssertExpectations(t)
		paymentClientGatewayMock.AssertCalled(t, "Create", mock.Anything, mock.Anything)

		updateOrderUseCaseMock.AssertExpectations(t)
		updateOrderUseCaseMock.AssertCalled(t, "Update", mock.Anything, mock.Anything, mock.Anything, mock.Anything)
	})
}
