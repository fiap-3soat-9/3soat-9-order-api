package command

import (
	"3soat-9-order-api/internal/domain/valueobject"
	"github.com/google/uuid"
)

type CreatePaymentStatusCommand struct {
	Id                uuid.UUID
	PaymentId         uuid.UUID
	ExternalReference uuid.UUID
	Status            valueobject.Status
}
