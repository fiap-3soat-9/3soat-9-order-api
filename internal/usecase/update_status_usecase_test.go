package usecase

import (
	"3soat-9-order-api/internal/domain"
	"3soat-9-order-api/internal/domain/valueobject"
	mocks "3soat-9-order-api/tests/mocks/port/output"
	"context"
	"errors"
	"github.com/stretchr/testify/mock"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func TestUpdateOrderUseCase_Update(t *testing.T) {

	orderId := uuid.New()
	paymentId := uuid.New()
	status := valueobject.OrderStatus("PAID")
	order := &domain.Order{
		Id:        orderId,
		Status:    valueobject.OrderStatus("PENDING"),
		UpdatedAt: time.Now(),
		History:   []domain.OrderHistory{},
	}

	ctx := context.Background()

	t.Run("Success", func(t *testing.T) {

		mockOrderPersistenceGateway := mocks.NewOrderPersistencePort(t)
		updateOrderUseCase := GetUpdateOrderUseCase(mockOrderPersistenceGateway)

		mockOrderPersistenceGateway.On("FindById", ctx, orderId).Return(order, nil)
		mockOrderPersistenceGateway.On("Update", ctx, mock.Anything).Return(nil)

		err := updateOrderUseCase.Update(ctx, orderId, status, &paymentId)

		assert.Nil(t, err)
		assert.Equal(t, status, order.Status)
		assert.Equal(t, paymentId, order.PaymentId)
		assert.NotEmpty(t, order.History)
		mockOrderPersistenceGateway.AssertExpectations(t)
	})

	t.Run("OrderNotFound", func(t *testing.T) {
		mockOrderPersistenceGateway := mocks.NewOrderPersistencePort(t)
		updateOrderUseCase := UpdateOrderUseCase{
			orderPersistenceGateway: mockOrderPersistenceGateway,
		}

		mockOrderPersistenceGateway.On("FindById", ctx, orderId).Return(nil, nil)

		err := updateOrderUseCase.Update(ctx, orderId, status, &paymentId)

		assert.NotNil(t, err)
		assert.Equal(t, "order not found", err.Error())
		mockOrderPersistenceGateway.AssertExpectations(t)
	})

	t.Run("PersistenceError", func(t *testing.T) {
		mockOrderPersistenceGateway := mocks.NewOrderPersistencePort(t)
		updateOrderUseCase := UpdateOrderUseCase{
			orderPersistenceGateway: mockOrderPersistenceGateway,
		}

		mockOrderPersistenceGateway.On("FindById", ctx, orderId).Return(nil, errors.New("database error"))

		err := updateOrderUseCase.Update(ctx, orderId, status, &paymentId)

		assert.NotNil(t, err)
		assert.Equal(t, "database error", err.Error())
		mockOrderPersistenceGateway.AssertExpectations(t)
	})
}
