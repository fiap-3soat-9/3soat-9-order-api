package usecase

import (
	"3soat-9-order-api/internal/domain"
	"3soat-9-order-api/internal/domain/events"
	"3soat-9-order-api/internal/domain/events/subscriber"
	"3soat-9-order-api/internal/domain/valueobject"
	"3soat-9-order-api/internal/port/input"
	"3soat-9-order-api/internal/port/output"
	"3soat-9-order-api/internal/usecase/command"
	"3soat-9-order-api/internal/usecase/result"
	"3soat-9-order-api/pkg/domainevent"
	"context"
	"errors"
	"fmt"
	"sync"
	"time"

	"github.com/google/uuid"
)

var (
	createOrderUseCaseInstance input.CreateOrderPort
	createOrderUseCaseOnce     sync.Once
)

type CreateOrderUseCase struct {
	orderPersistenceGateway  output.OrderPersistencePort
	productClientGateway     output.ProductClientGatewayPort
	processPaymentUseCase    input.ProcessPaymentPort
	OrderCreatedEventHandler subscriber.OrderCreatedEventHandler
	domainEventDispatcher    domainevent.EventDispatcher[events.OrderCreatedDomainEvent]
}

func (c CreateOrderUseCase) AddOrder(
	ctx context.Context,
	createOrderCommand command.CreateOrderCommand,
) (*result.CreateOrderResult, error) {
	var amount int
	var products []domain.OrderProduct
	orderId := uuid.New()

	for _, createProductCommand := range createOrderCommand.Products {
		var productAmount int
		if createProductCommand.Type == "default" {
			product, err := c.productClientGateway.GetByNumber(ctx, createProductCommand.Number)
			if err != nil {
				return nil, err
			}
			if product == nil {
				return nil, errors.New(fmt.Sprintf("product %d not found", createProductCommand.Number))
			}
			productAmount = product.Amount * createProductCommand.Quantity
			products = append(products, domain.OrderProduct{
				Id:       uuid.New(),
				Product:  *product,
				OrderId:  orderId,
				Quantity: createProductCommand.Quantity,
				Amount:   productAmount,
			})
		} else {
			return nil, errors.New("invalid product type")
		}

		amount = amount + productAmount

	}

	order := domain.Order{
		Id:         orderId,
		CustomerId: createOrderCommand.CustomerDocument,
		Products:   products,
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
		Status:     valueobject.Created,
		Amount:     amount,
	}

	err := c.orderPersistenceGateway.Create(ctx, order)
	if err != nil {
		return nil, err
	}

	orderFound, err := c.orderPersistenceGateway.FindById(ctx, orderId)
	if err != nil {
		return nil, err
	}

	//paymentCreated, err := c.processPaymentUseCase.ProcessPayment(ctx, *orderFound)
	//if err != nil {
	//	return nil, err
	//}

	event := events.OrderCreatedDomainEvent{
		Id:         orderFound.Id,
		Number:     orderFound.Number,
		CustomerId: orderFound.CustomerId,
		Products:   products,
		CreatedAt:  orderFound.CreatedAt,
		UpdatedAt:  orderFound.UpdatedAt,
		Status:     orderFound.Status,
		Amount:     orderFound.Amount,
	}.ToDomainEvent()

	errChan := c.domainEventDispatcher.Dispatch(event)
	for errC := range errChan {
		if errC != nil {
			fmt.Printf("Error handling event: %v\n", err)
		}
	}

	//err = c.OrderCreatedEventHandler.Handle(ctx, event)
	//if err != nil {
	//	return nil, err
	//}

	return &result.CreateOrderResult{
		Number:      orderFound.Number,
		Amount:      amount,
		PaymentData: "",
	}, err
}

func GetCreateOrderUseCase(
	orderPersistenceGateway output.OrderPersistencePort,
	productClientGateway output.ProductClientGatewayPort,
	processPaymentUseCase input.ProcessPaymentPort,
	//OrderCreatedEventHandler subscriber.OrderCreatedEventHandler,
	domainEventDispatcher domainevent.EventDispatcher[events.OrderCreatedDomainEvent],
) input.CreateOrderPort {
	createOrderUseCaseOnce.Do(func() {
		createOrderUseCaseInstance = CreateOrderUseCase{
			orderPersistenceGateway: orderPersistenceGateway,
			productClientGateway:    productClientGateway,
			processPaymentUseCase:   processPaymentUseCase,
			//OrderCreatedEventHandler: OrderCreatedEventHandler,
			domainEventDispatcher: domainEventDispatcher,
		}
	})
	return createOrderUseCaseInstance
}
