CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS "order" (
    id              UUID not null primary key default public.uuid_generate_v4(),
    number          serial,
    customer_id     varchar(80),
    payment_id      UUID,
    takeAway        boolean not null default false,
    amount          bigint not null,
    status          varchar(50) not null ,
    created_at      timestamp not null,
    updated_at      timestamp
);

CREATE TABLE IF NOT EXISTS "order_product" (
    id                      UUID not null primary key default public.uuid_generate_v4(),
    product_number          serial,
    order_id                UUID references "order"(id) not null,
    quantity                int not null,
    amount                  bigint not null
);


CREATE TABLE IF NOT EXISTS "order_history" (
    id          UUID not null primary key default public.uuid_generate_v4(),
    order_id    UUID references "order"(id) not null,
    status      varchar(50) not null,
    change_by   varchar(50) not null,
    created_at  timestamp not null
);

CREATE TABLE IF NOT EXISTS "payment_requests" (
    id UUID not null primary key default public.uuid_generate_v4(),
    reference_id varchar(255) not null,
    order_id UUID REFERENCES "order"(id),
    amount BIGINT NOT NULL,
    status VARCHAR(50) NOT NULL,
    attempts INT NOT NULL DEFAULT 0,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "outbox" (
    id UUID not null primary key default public.uuid_generate_v4(),
    aggregate_id UUID,
    type VARCHAR(50) NOT NULL,
    payload TEXT NOT NULL,
    status VARCHAR(50) NOT NULL,
    error_message VARCHAR(255),
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
    );

CREATE UNIQUE INDEX IF NOT EXISTS order_number_unq_idx ON "order"(number);
CREATE INDEX IF NOT EXISTS idx_outbox_aggregate_id ON outbox(aggregate_id);
CREATE INDEX IF NOT EXISTS idx_payment_requests_order_id ON payment_requests(order_id);


ALTER TABLE "order_history" ADD CONSTRAINT fk_order_history_order
    FOREIGN KEY (order_id) REFERENCES "order"(id);

