DO $$
 BEGIN
    IF EXISTS (SELECT rolname FROM pg_roles where rolname like 'soatorder') THEN
	 SET ROLE "soatorder";
	end if;
  end;
$$;

CREATE SCHEMA IF NOT EXISTS soatorder;
SET STATEMENT_TIMEOUT TO '300s';
