#!/bin/bash
set -e
set -u

function create_user_and_database() {
    local dbname=$1
    local username=$3
    local applicationUser="${username}-app"
    local applicationReadUser="${username}-app-read"
    local applicationWriteUser="${username}-app-write"
    local migrationUser="${username}-migration"
    local schema=$2
    echo "  Creating database '$dbname' with schema '$schema' and user ['$applicationReadUser', '$applicationWriteUser', '$migrationUser']"

    psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
        CREATE ROLE "$username";
        CREATE DATABASE "$dbname" WITH OWNER '$username';
        GRANT ALL PRIVILEGES ON DATABASE "$dbname" TO "$username";

        CREATE USER "$applicationUser" WITH PASSWORD '$applicationUser';
        GRANT CONNECT ON DATABASE "$dbname" TO "$applicationUser";
        GRANT "$username" TO "$applicationUser";

        CREATE USER "$applicationReadUser" WITH PASSWORD '$applicationReadUser';
        GRANT CONNECT ON DATABASE "$dbname" TO "$applicationReadUser";
        GRANT "$username" TO "$applicationReadUser";

        CREATE USER "$applicationWriteUser" WITH PASSWORD '$applicationWriteUser';
        GRANT CONNECT ON DATABASE "$dbname" TO "$applicationWriteUser";
        GRANT "$username" TO "$applicationWriteUser";

        CREATE USER "$migrationUser" WITH PASSWORD '$migrationUser';
        GRANT CONNECT ON DATABASE "$dbname" TO "$migrationUser";
        GRANT "$username" TO "$migrationUser";
EOSQL

    psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" -d "$dbname"<<-EOSQL
        CREATE SCHEMA "$schema" AUTHORIZATION "$username";
        CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;
        ALTER ROLE "$username" SET search_path TO "$schema", public;
        ALTER ROLE "$applicationUser" SET search_path TO "$schema", public;
        ALTER ROLE "$applicationReadUser" SET search_path TO "$schema", public;
        ALTER ROLE "$applicationWriteUser" SET search_path TO "$schema", public;
        ALTER ROLE "$migrationUser" SET search_path TO "$schema", public;

        -- Grant all privileges on all tables, sequences, and functions in the schema
        GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA "$schema" TO "$username";
        GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA "$schema" TO "$username";
        GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA "$schema" TO "$username";

        -- Ensure future tables will have the same privileges
        ALTER DEFAULT PRIVILEGES IN SCHEMA "$schema" GRANT ALL PRIVILEGES ON TABLES TO "$username";
        ALTER DEFAULT PRIVILEGES IN SCHEMA "$schema" GRANT ALL PRIVILEGES ON SEQUENCES TO "$username";
        ALTER DEFAULT PRIVILEGES IN SCHEMA "$schema" GRANT ALL PRIVILEGES ON FUNCTIONS TO "$username";
EOSQL
}

echo "Creating database soatorder"

if [ -n "$POSTGRES_DB" ]; then
    create_user_and_database "soatorder" "soatorder" "soat-order"
fi

echo "Finished setup-postgresql-databases.sh"
