package config

type Queues struct {
	PaymentUpdated string
	PaymentCreated string
}
