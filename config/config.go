package config

var configInstance *Root

func SetConfig(c *Root) {
	configInstance = c
}

func GetConfig() *Root {
	return configInstance
}
