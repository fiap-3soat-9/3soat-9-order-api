package config

type SNSConfig struct {
	Topic
}

type Topic struct {
	Name string
	Arn  string
}

type SNSTopics struct {
	Topics Topics
}

type Topics struct {
	PaymentEvents Topic
	OrderEvents   Topic
}
