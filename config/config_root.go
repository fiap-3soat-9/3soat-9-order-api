package config

type Root struct {
	Application    Application
	HttpServer     HttpServerConfig
	Aws            AwsConfig
	Databases      map[string]DatabaseConfig
	HttpClients    map[string]HttpClientConfig
	PaymentGateway PaymentGateway
	ProductGateway ProductGateway
	Queues         Queues
	Sns            SNSTopics
}
