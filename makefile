.PHONY: setup create-sqs create-sns create-events


AWSLOCAL = aws --endpoint-url=http://localhost:4566
ARN=arn:aws:sns:us-east-1:000000000000
ENDPOINT=http://localstack:4566/000000000000


# Queue names
ORDER_CREATED_QUEUE = OrderCreated

PAYMENT_CREATE_COMMAND_QUEUE = paymentCreateCommand
PAYMENT_CREATED_QUEUE = paymentCreated
PAYMENT_UPDATED_QUEUE = paymentUpdated

KITCHEN_PREPARE_ORDER_COMMAND_QUEUE = KitchenPrepareOrderCommand
KITCHEN_ORDER_PREPARED_QUEUE = KitchenOrderPrepared

CUSTOMER_NOTIFICATION_QUEUE = CustomerNotification



# Topic names
ORDER_TOPIC = ORDER_EVENTS
PAYMENT_TOPIC = PAYMENT_EVENTS
KITCHEN_TOPIC = KITCHEN_EVENTS
CUSTOMER_TOPIC = CUSTOMER_EVENTS


tools:
	@echo "Installing tools..."
	go get -u github.com/golang-migrate/migrate


# Default make target
messaging-setup: create-sqs create-sns sub-sns

# Create SQS queues
create-sqs:
	@echo "Creating SQS Queues..."
	$(AWSLOCAL) sqs create-queue --queue-name $(ORDER_CREATED_QUEUE)
	$(AWSLOCAL) sqs create-queue --queue-name $(PAYMENT_CREATED_QUEUE)
	$(AWSLOCAL) sqs create-queue --queue-name $(PAYMENT_CREATE_COMMAND_QUEUE)
	$(AWSLOCAL) sqs create-queue --queue-name $(PAYMENT_UPDATED_QUEUE)


	$(AWSLOCAL) sqs create-queue --queue-name $(KITCHEN_PREPARE_ORDER_COMMAND_QUEUE)
	$(AWSLOCAL) sqs create-queue --queue-name $(KITCHEN_ORDER_PREPARED_QUEUE)
	$(AWSLOCAL) sqs create-queue --queue-name $(CUSTOMER_NOTIFICATION_QUEUE)


# Create SNS topics and subscriptions to SQS queues
create-sns:
	@echo "Creating SNS Topics and linking to SQS..."
	$(AWSLOCAL) sns create-topic --name $(ORDER_TOPIC)
	$(AWSLOCAL) sns create-topic --name $(PAYMENT_TOPIC)
	$(AWSLOCAL) sns create-topic --name $(KITCHEN_TOPIC)
	$(AWSLOCAL) sns create-topic --name $(CUSTOMER_TOPIC)
#
sub-sns:
	@echo "Subscribing SNS Topics to SQS Queues..."
	$(AWSLOCAL) sns subscribe --topic-arn $(ARN):$(ORDER_TOPIC) --protocol sqs --notification-endpoint $(ARN):$(ORDER_CREATED_QUEUE) --attributes '{"FilterPolicy":"{\"event_type\":[\"order.created\"]}", "RawMessageDelivery":"true"}'; \
	$(AWSLOCAL) sns subscribe --topic-arn $(ARN):$(PAYMENT_TOPIC) --protocol sqs --notification-endpoint $(ARN):$(PAYMENT_CREATE_COMMAND_QUEUE) --attributes '{"FilterPolicy":"{\"event_type\":[\"payment.create\"]}", "RawMessageDelivery":"true"}'; \
	$(AWSLOCAL) sns subscribe --topic-arn $(ARN):$(PAYMENT_TOPIC) --protocol sqs --notification-endpoint $(ARN):$(PAYMENT_CREATED_QUEUE) --attributes '{"FilterPolicy":"{\"event_type\":[\"payment.created\"]}", "RawMessageDelivery":"true"}'; \
	$(AWSLOCAL) sns subscribe --topic-arn $(ARN):$(PAYMENT_TOPIC) --protocol sqs --notification-endpoint $(ARN):$(PAYMENT_UPDATED_QUEUE) --attributes '{"FilterPolicy":"{\"event_type\":[\"payment.approved\",\"payment.rejected\"]}", "RawMessageDelivery":"true"}'; \
	$(AWSLOCAL) sns subscribe --topic-arn $(ARN):$(KITCHEN_TOPIC) --protocol sqs --notification-endpoint $(ARN):$(KITCHEN_PREPARE_ORDER_COMMAND_QUEUE) --attributes '{"FilterPolicy":"{\"event_type\":[\"kitcher.approved\",\"payment.rejected\"]}", "RawMessageDelivery":"true"}'; \
	$(AWSLOCAL) sns subscribe --topic-arn $(ARN):$(KITCHEN_TOPIC) --protocol sqs --notification-endpoint $(ARN):$(KITCHEN_ORDER_PREPARED_QUEUE) --attributes '{"FilterPolicy":"{\"event_type\":[\"payment.approved\",\"payment.rejected\"]}", "RawMessageDelivery":"true"}'; \
	$(AWSLOCAL) sns subscribe --topic-arn $(ARN):$(CUSTOMER_TOPIC) --protocol sqs --notification-endpoint $(ARN):$(CUSTOMER_NOTIFICATION_QUEUE) --attributes '{"FilterPolicy":"{\"event_type\":[\"payment.approved\",\"payment.rejected\"]}", "RawMessageDelivery":"true"}'




clean:
	@echo "Cleaning up all resources..."
	$(AWSLOCAL) sns delete-topic --topic-arn `$(AWSLOCAL) sns list-topics | jq -r '.Topics[] | select(.TopicArn | contains("$(ORDER_TOPIC)")) | .TopicArn'`
	$(AWSLOCAL) sns delete-topic --topic-arn `$(AWSLOCAL) sns list-topics | jq -r '.Topics[] | select(.TopicArn | contains("$(PAYMENT_TOPIC)")) | .TopicArn'`
	$(AWSLOCAL) sqs delete-queue --queue-url `$(AWSLOCAL) sqs get-queue-url --queue-name $(ORDER_QUEUE) | jq -r '.QueueUrl'`

	$(AWSLOCAL) sqs delete-queue --queue-url `$(AWSLOCAL) sqs get-queue-url --queue-name $(PAYMENT_CREATED_QUEUE) | jq -r '.QueueUrl'`
	$(AWSLOCAL) sqs delete-queue --queue-url `$(AWSLOCAL) sqs get-queue-url --queue-name $(PAYMENT_UPDATED_QUEUE) | jq -r '.QueueUrl'`
	$(AWSLOCAL) sqs delete-queue --queue-url `$(AWSLOCAL) sqs get-queue-url --queue-name $(PAYMENT_CREATE_COMMAND_QUEUE) | jq -r '.QueueUrl'`

	$(AWSLOCAL) sqs delete-queue --queue-url `$(AWSLOCAL) sqs get-queue-url --queue-name $(KITCHEN_PREPARE_ORDER_COMMAND_QUEUE) | jq -r '.QueueUrl'`
	$(AWSLOCAL) sqs delete-queue --queue-url `$(AWSLOCAL) sqs get-queue-url --queue-name $(KITCHEN_ORDER_PREPARED_QUEUE) | jq -r '.QueueUrl'`

	$(AWSLOCAL) sqs delete-queue --queue-url `$(AWSLOCAL) sqs get-queue-url --queue-name $(CUSTOMER_NOTIFICATION_QUEUE) | jq -r '.QueueUrl'`

