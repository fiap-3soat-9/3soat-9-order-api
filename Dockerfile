FROM golang:1.22-alpine as base

WORKDIR /3soat-9-order-api

# Download Go modules
COPY go.mod go.sum ./
RUN go mod download

# BUILDER
FROM base as builder

COPY . /3soat-9-order-api

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64  go build -ldflags "${LDFLAGS} -s -w" -a -o order-api /3soat-9-order-api/cmd/main.go

FROM scratch

WORKDIR /3soat-9-order-api

COPY --from=builder /3soat-9-order-api/config /3soat-9-order-api/config
COPY --from=builder /3soat-9-order-api/order-api /3soat-9-order-api


EXPOSE 8080 8081


CMD ["./order-api"]
